angular.module('ibuysell', ['ionic', 'firebase', 'ionic-toast','ngStorages'])
        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })
        .config(function($stateProvider, $urlRouterProvider) {
            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'js/module/product/templates/categorysidebar.html',
                        controller: 'productSidebarController'
                    })
                    .state('app.home', {
                        url: '/home',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/home/templates/home.html',
                                controller: 'HomeController as hm'
                            }
                        }
                    })
                    .state('app.productDetails', {
                        url: '/productdetails',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/productdetails.html',
                                controller: 'productDetailscontroller as pd'
                            }
                        }
                    })
                    .state('app.login', {
                        url: '/login',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/login.html',
                                controller: 'LoginController as lc'
                            }
                        }
                    })
                    .state('app.myaccount', {
                        url: '/myaccount',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myaccount.html',
                                controller: 'MyAccountController as ma'
                            }
                        }
                    })
                    .state('app.registration', {
                        url: '/registration',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/registration.html',
                                controller: 'RegistrationController as rc'
                            }
                        }
                    });
            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/app/home');
        })
        .factory('serverUrls', serverUrls);
serverUrls.$inject = [];
function serverUrls() {
    var HOSTURL = 'http://59.162.181.92/dtswork/Ibuysell/index.php';
    var urls = {
        login: HOSTURL + '/Login/api_userloginprocess',
        register: HOSTURL + '/Login/api_newuserregistration',
        forgot: HOSTURL + '/forgotpassword'
    };
    return urls;
}
