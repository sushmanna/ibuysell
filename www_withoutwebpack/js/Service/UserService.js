(function () {
    angular.module('ibuysell').factory('UserService', UserService);
    UserService.$inject = ['$http', 'serverUrls', '$window', '$location'];
    function UserService($http, serverUrls, $window, $location) {
        var data = {};
//        data.register = function(userData) {
//            return $http({
//                url: serverUrls.register,
//                method: 'POST',
//                data: userData,
//                responseText: 'JSON'
//            });
//
//        };
        data.login = function (userData) {
            return $http({
                url: serverUrls.login,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
//        data.forgotpassword = function(userData) {
//            return $http({
//                url: serverUrls.forgot,
//                method: 'POST',
//                data: userData,
//                responseText: 'JSON'
//            });
//        };
//        data.logout = function() {
//            return $http({
//                url: serverUrls.logout,
//                method: 'GET',
//                responseText: 'JSON'
//            });
//        };
//        data.isUserLogin = function() {
//            return $http({
//                url: serverUrls.isUserLogin,
//                method: 'GET',
//                responseText: 'JSON'
//            });
//        };
//        data.loginRedirect = function(data) {
//            var access = data.role.indexOf("admin");
//            if (access != -1)
//            {
//                $window.location.href = '/admin';
//            } else
//            {
//                $window.location.href = '/user/dashboard';
//            }
//        };
//        data.getProfile = function(userId) {
//            return $http({
//                url: serverUrls.profile + '/' + userId,
//                method: 'GET',
//                responseText: 'JSON'
//            });
//        };
//        data.updateProfile = function(data) {
//            return $http({
//                url: serverUrls.profile,
//                method: 'PUT',
//                data: data,
//                responseText: 'JSON'
//            });
//        };
//        data.contactUs = function(data) {
//            return $http({
//                url: serverUrls.sendmail,
//                method: 'POST',
//                data: data,
//                responseText: 'JSON'
//            });
//        };
        return data;
    }
})();
