define(function(require, exports, module) {
    //js lib
    require('file?name=lib/ionic/js/ionic.bundle.min.js!./lib/ionic/js/ionic.bundle.min.js');
    require('file?name=lib/firebase/firebase.js!./lib/firebase/firebase.js');
    require('file?name=lib/swiper/dist/js/swiper.min.js!./lib/swiper/dist/js/swiper.min.js');
    require('file?name=lib/ionic-toast/dist/ionic-toast.bundle.min.js!./lib/ionic-toast/dist/ionic-toast.bundle.min.js');
    require('file?name=lib/ngstorage/ngStorage.min.js!./lib/ngstorage/ngStorage.min.js');
    require('file?name=lib/ngCordova/dist/ng-cordova.min.js!./lib/ngCordova/dist/ng-cordova.min.js');
    require('file?name=/lib/jquery/dist/jquery.min.js!./lib/jquery/dist/jquery.min.js');
    require('file?name=/lib/ionic-datepicker/dist/ionic-datepicker.bundle.min.js!./lib/ionic-datepicker/dist/ionic-datepicker.bundle.min.js');
    require('file?name=/lib/ionic-timepicker/dist/ionic-timepicker.bundle.min.js!./lib/ionic-timepicker/dist/ionic-timepicker.bundle.min.js');
    require('file?name=/lib/jquery-ui-1.11.4.custom/jquery-ui.js!./lib/jquery-ui-1.11.4.custom/jquery-ui.js');
    require('file?name=/lib/underscore/underscore-min.js!./lib/underscore/underscore-min.js');
    //require('file?name=/lib/ionic-rating/ionic-rating.min.js!./lib/ionic-rating/ionic-rating.min.js');


    //angular lib
    require('./lib/angularfire/dist/angularfire.min.js');
    require('./lib/angular-swiper/dist/angular-swiper.js');
    require('./lib/angular-underscore-module/angular-underscore-module.js');
    require('./lib/ionic-rating/ionic-rating.min.js');
    // require('./lib/ion-autocomplete/dist/ion-autocomplete.min.js');


    //image
    require('./img/banner01.jpg');
    require('./img/banner02.jpg');
    require('./img/banner03.jpg');
    require('./img/banner04.jpg');
    require('./img/home.png');
    require('./img/top-logo.png');
    require('./img/user-img.png');
    //require('./img/blue-album.jpg');
    require('./img/loader.GIF');
    require('./img/user-no-img.png');
    require('./img/product-no-img.png');
    require('./img/upload-no-img.png');
    //require('./img/pro-thumb.jpg');
    //require('./img/pro-dtl-img.jpg');
    //require('./img/wishlist-img.jpg');
    
    //sound
    require('./sound/notification.mp3');
     
    //lauout
    require('./index.html');

    //main sctipt
    require('./js/app.js');

    //Service
    require('./js/Service/UserService.js');
    require('./js/Service/ProductService.js');
    require('./js/Service/NotificationService.js');
    require('./js/Service/FirebaseService.js');
    require('./js/Service/DataService.js');
    require('./js/Service/ShipstationService.js');
    
    //Filter
    require('./js/Filters/AllFilter.js');


    //Directive
    require('./js/Directives/accountTopDirective.js');
    require('./js/Directives/slideToggleDirective.js');
    require('./js/Directives/accountFooterDirective.js');
    require('./js/Directives/categorysliderDirective.js');
    require('./js/Directives/progressbarDirective.js');
    require('./js/Directives/listProductDirective.js');
    require('./js/Directives/linkImageDirective.js');
    require('./js/Directives/addToCartDirective.js');
    require('./js/Directives/managePresetDirective.js');
    //require('./js/Directives/imageCheck.js');
    //template
    require('./js/Directives/templates/accounttop.html');
    require('./js/Directives/templates/accountfooter.html');
    require('./js/Directives/templates/categoryslider.html');
    require('./js/Directives/templates/listProduct.html');
    require('./js/Directives/templates/slideToggle.html');

    //module start
    //home module
    require('./js/module/home/controllers/HomeController.js');
    require('./js/module/home/controllers/CmsController.js');
    require('./js/module/home/controllers/FaqController.js');
    require('./js/module/home/controllers/HelpController.js');
    //template
    require('./js/module/home/templates/home.html');
    require('./js/module/home/templates/cms.html');
    require('./js/module/home/templates/faq.html');
    require('./js/module/home/templates/help.html');
    require('./js/module/home/templates/faqModal.html');

    //product module
    require('./js/module/product/controllers/productSidebarController.js');
    require('./js/module/product/controllers/categoryallController.js');
    require('./js/module/product/controllers/livesaleController.js');
    require('./js/module/product/controllers/uploadProductController.js');
    require('./js/module/product/controllers/searchController.js');
    require('./js/module/product/controllers/productDetailsController.js'); 
    require('./js/module/product/controllers/liveSaleListController.js');
    require('./js/module/product/controllers/categoryLivesaleController.js');
    require('./js/module/product/controllers/tagListController.js');
    require('./js/module/product/controllers/cartController.js');
    require('./js/module/product/controllers/checkoutController.js');
    require('./js/module/product/controllers/paymentController.js');
    require('./js/module/product/controllers/productListController.js');
    
    //template
    require('./js/module/product/templates/categorysidebar.html');
    require('./js/module/product/templates/productdetails.html');
    require('./js/module/product/templates/livesalelist.html');
    require('./js/module/product/templates/categoryLivesale.html');
    require('./js/module/product/templates/categoryall.html'); 
    require('./js/module/product/templates/livesale.html');
    require('./js/module/product/templates/uploadProduct.html');
    require('./js/module/product/templates/search.html');
    require('./js/module/product/templates/taglist.html');
    require('./js/module/product/templates/payment.html');
    require('./js/module/product/templates/cart.html');
    require('./js/module/product/templates/checkout.html');
    require('./js/module/product/templates/productlist.html');
    require('./js/module/product/templates/presetModal.html');
    require('./js/module/product/templates/messageModal.html');


    //user module
    require('./js/module/user/controllers/LoginController.js');
    require('./js/module/user/controllers/RegistrationController.js');
    require('./js/module/user/controllers/MyAccountController.js');
    require('./js/module/user/controllers/MyAccountSettingsController.js');
    require('./js/module/user/controllers/ShareAppController.js');
    require('./js/module/user/controllers/NotificationController.js');
    require('./js/module/user/controllers/MyClosetController.js');
    require('./js/module/user/controllers/MyAddressController.js');
    require('./js/module/user/controllers/SubscriptionController.js');
    require('./js/module/user/controllers/MyOrderController.js');
    require('./js/module/user/controllers/SoldItemController.js');
    require('./js/module/user/controllers/MyFeedbackController.js');
    require('./js/module/user/controllers/CustomerSupportController.js');
    require('./js/module/user/controllers/TutorialController.js'); 
    require('./js/module/user/controllers/InboxController.js');
    require('./js/module/user/controllers/MessageDetailsController.js');
    require('./js/module/user/controllers/SubscriptionPaymentController.js');
    require('./js/module/user/controllers/SubscriptionDetailsController.js');
    
    //template
    require('./js/module/user/templates/login.html');
    require('./js/module/user/templates/myaccount.html');
    require('./js/module/user/templates/myaccountsetting.html');
    require('./js/module/user/templates/registration.html');
    require('./js/module/user/templates/emailModal.html');
    require('./js/module/user/templates/passwordModal.html');
    require('./js/module/user/templates/shareapp.html');
    require('./js/module/user/templates/notification.html');
    require('./js/module/user/templates/mycloset.html');
    require('./js/module/user/templates/myaddress.html');
    require('./js/module/user/templates/subscription.html');
    require('./js/module/user/templates/subscriptionmodal.html');
    require('./js/module/user/templates/myorder.html');
    require('./js/module/user/templates/solditem.html');
    require('./js/module/user/templates/ratingModal.html');
    require('./js/module/user/templates/myfeedback.html');
    require('./js/module/user/templates/customersupport.html');
    require('./js/module/user/templates/tutorial.html');
    require('./js/module/user/templates/messagedetails.html');
    require('./js/module/user/templates/inbox.html');
    require('./js/module/user/templates/subscriptionpayment.html');
    require('./js/module/user/templates/shareMsgModal.html');
    require('./js/module/user/templates/creditModal.html');
    require('./js/module/user/templates/subscriptiondetails.html');
});