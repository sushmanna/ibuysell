liveSaleTime = 90;
freezeTime = 900;
livesalecatdata = {};
productdata = {};
angular.module('ibuysell', ['ionic', 'firebase', 'ionic-toast', 'ngStorage', 'ksSwiper', 'ngCordova', 'ionic-timepicker', 'underscore', 'ionic.rating', 'ionic-datepicker'])
        .run(function($ionicPlatform, $localStorage, $location, $rootScope, ionicToast, $timeout, $interval, $cordovaPush, $cordovaDevice, $ionicPopup, UserService, NotificationService,$ionicLoading, $ionicHistory) {
            $ionicPlatform.ready(function() {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }

                //check login
                if ($localStorage.loginData) {
                    $rootScope.loginData = $localStorage.loginData;
                    NotificationService.getMessageNotifications($rootScope.loginData.user_ID);
                    NotificationService.getLocalNotifications($rootScope.loginData.user_ID);
                    NotificationService.getAlertNotifications($rootScope.loginData.user_ID);
                }

                //check connectionhty
                if (window.Connection) {
                    if (navigator.connection.type == Connection.NONE) {
                        ionicToast.show('Error: Please check your network connection.', 'middle', false, 4000);
                        $timeout(function() {
                            ionic.Platform.exitApp();
                        }, 5000);
                    }
                }

                if( window.cordova){
                    if( $cordovaDevice.getPlatform().toLowerCase() == 'android'){
                        //var androidConfig = { "senderID": "1082308898211"};
                        var androidConfig = { "senderID": "479408279555"};
                        $cordovaPush.register(androidConfig).then(function(result) {
                            console.log('success');
                        }, function(err) {
                            console.log('error');
                        });
                    }
                    else if( $cordovaDevice.getPlatform().toLowerCase() == 'ios'){
                        var iosConfig = { "badge": true, "sound": true, "alert": true};
                        $cordovaPush.register(iosConfig).then(function(deviceToken) {
                          $localStorage.device_details = { device_type: $cordovaDevice.getPlatform().toLowerCase(), device_token: deviceToken};
                        }, function(err) {
                            console.log('Error: '+err);
                        });
                    }
                } else{
                    //console.log('browser');
                }

                $ionicPlatform.registerBackButtonAction(function(event) {
                    if ( $ionicHistory.currentStateName() == 'app.home' || $ionicHistory.currentStateName() == 'app.login' || $ionicHistory.currentStateName() == 'app.livesalelist') {
                        $ionicPopup.confirm({
                            title: 'See you soon.',
                            template: 'Are you sure you want to exit?'
                        }).then(function(res) {
                            if (res) {
                                ionic.Platform.exitApp();
                            }
                        })
                    } else {
                        return false;
                        //$ionicHistory.goBack();
                    }
                }, 100);
                
            });
    
            $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
                // This section for ANDROID push notification callback
                if( $cordovaDevice.getPlatform().toLowerCase() == 'android'){
                    switch(notification.event) {
                        case 'registered':
                            if (notification.regid.length > 0 ) {
                                $localStorage.device_details = { device_type: $cordovaDevice.getPlatform().toLowerCase(), device_token: notification.regid}; 
                            }
                        break;

                        case 'message':
                            //console.log( JSON.stringify(notification));
                            //var payld = notification.payload;
                            processNotification( notification.payload);
                        break;

                        case 'error':
                            console.log('GCM error = ' + notification.msg);
                        break;

                        default:
                            console.log('An unknown GCM event has occurred');
                        break;
                    }
                }
                
                // This section for IOS push notification callback
                if( $cordovaDevice.getPlatform().toLowerCase() == 'ios'){
                    /*if (notification.badge) {
                        $cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
                                console.log( "Result:"+result);
                            }, function(err) {
                                // An error occurred. Show a message to the user
                                console.log( "Error:"+err);
                        });
                    }*/

                    if (notification.alert){
                        processNotification( notification);
                    }

                    /*if (notification.sound) {
                        var snd = new Media(event.sound);
                        snd.play();
                    }*/
                }

                function processNotification( payld){
                    var popuptitle = ( payld.popuptitle && payld.popuptitle != '') ? payld.popuptitle : 'Welcome to iBuySell';
                    var popupsubtitle = ( payld.popupsubtitle && payld.popupsubtitle != '') ? payld.popupsubtitle : 'Check the offer';
                    var popupbody = ( payld.popupbody && payld.popupbody != '') ? payld.popupbody : "Don't miss the existing offer";
                    var canceltext = ( payld.canceltext && payld.canceltext != '') ? payld.canceltext : 'Cancel';
                    var oktext = ( payld.oktext && payld.oktext != '') ? payld.oktext : 'OK';
                    if ($localStorage.loginData) {
                        var redirectpath = ( payld.redirectpath && payld.redirectpath != '') ? payld.redirectpath : '/app/categoryall';
                    } else {
                        var redirectpath = '/app/login';
                    }                        

                    if( payld.showconfirmpopup && payld.showconfirmpopup == 1){
                        $ionicPopup.confirm({
                            title: popuptitle,
                            subTitle: popupsubtitle,
                            template: popupbody,
                            cssClass: 'push-popup-wrap',
                            cancelText: canceltext,
                            okText: oktext,
                        }).then(function(res) {
                            if (res) {
                                $location.path(redirectpath);
                            } else {
                                console.log('failure');
                            }
                        });
                    }
                    else if( payld.showalertpopup && payld.showalertpopup == 1){
                        var alertPopup = $ionicPopup.alert({
                            title: popuptitle,
                            subTitle: popupsubtitle,
                            template: popupbody,
                            cssClass: 'push-popup-wrap',
                            okText: oktext
                        });
                        alertPopup.then(function(res) {
                            $location.path(redirectpath);
                        });
                    } else {
                        $location.path(redirectpath);
                    }
                }               
            });
            

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                /*for registration message and off for page*/
                if (toState.name != 'app.login' && toState.name != 'app.mycloset' && toState.name != 'app.subscription' && toState.name != 'app.cart') {
                    ionicToast.hide();
                }
                // hide if loader in on 
                $ionicLoading.hide();
                /*get the noral page without login*/
                var loginState = ['app.login', 'app.registration', 'app.home'];
                var state = loginState.indexOf(toState.name);

                /*save the last state when login*/
                if (state == -1 && $localStorage.loginData)
                {
                    $localStorage.lastState = toState.name;
                    $localStorage.lastStateUrl = toState.url;
                }

                /*restrict loginState array when logoged in */
                if (state != -1 && $localStorage.loginData)
                {
                    $rootScope.loginData = $localStorage.loginData;
                }
            });
        })
        .config(function($stateProvider, $urlRouterProvider) {
            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'js/module/product/templates/categorysidebar.html',
                        controller: 'productSidebarController as ps'
                    })
                    .state('app.home', {
                        url: '/home',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/home/templates/home.html',
                                controller: 'HomeController as hm'
                            }
                        }
                    })
                    .state('app.help', {
                        url: '/help',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/home/templates/help.html',
                                controller: 'HelpController as help'
                            }
                        }
                    })
                    .state('app.cms', {
                        url: '/cms/:cmsKey',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/home/templates/cms.html',
                                controller: 'CmsController as cms'
                            }
                        }
                    })
                    .state('app.faq', {
                        url: '/faq',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/home/templates/faq.html',
                                controller: 'FaqController as faq'
                            }
                        }
                    })
//                    .state('app.livesalelist', {
//                        url: '/livesalelist',
//                        views: {
//                            'menuContent': {
//                                templateUrl: 'js/module/product/templates/livesale.html',
//                                controller: 'livesaleController as ls'
//                            }
//                        }
//                    })
                    .state('app.livesalelistcat', {
                        url: '/livesalelistcat',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/livesalelist.html',
                                controller: 'liveSaleListController as lsl'
                            }
                        }
                    })
                    .state('app.livesalecat', {
                        url: '/livesalecat/:catId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/categoryLivesale.html',
                                controller: 'categoryLivesaleController as cls'
                            }
                        }
                    })
                    .state('app.livesalelist', {
                        url: '/livesalelist',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/livesale.html',
                                controller: 'livesaleController as ls'
                            }
                        }
                    })
                    .state('app.categoryall', {
                        url: '/categoryall',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/categoryall.html',
                                controller: 'categoryallController as ls'
                            }
                        }
                    })
                    .state('app.productdetail', {
                        url: '/productdetail/:firebaseId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/productdetails.html',
                                controller: 'productDetailsController as pd'
                            }
                        }
                    })
                    .state('app.uploadproduct', {
                        url: '/uploadproduct',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/uploadProduct.html',
                                controller: 'uploadProductController as up'
                            }
                        }
                    })
                    .state('app.productlist', {
                        url: '/productlist/:catId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/productlist.html',
                                controller: 'productListController as list'
                            }
                        }
                    })
                    .state('app.search', {
                        url: '/search',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/search.html',
                                controller: 'searchController as sc'
                            }
                        }
                    })
                    .state('app.taglist', {
                        url: '/taglist',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/taglist.html',
                                controller: 'tagListController as tl'
                            }
                        }
                    })
                    .state('app.cart', {
                        url: '/cart',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/cart.html',
                                controller: 'cartController as cart'
                            }
                        }
                    })
                    .state('app.checkout', {
                        url: '/checkout',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/checkout.html',
                                controller: 'checkoutController as chk'
                            }
                        }
                    })
                    .state('app.payment', {
                        url: '/payment',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/payment.html',
                                controller: 'paymentController as pay'
                            }
                        }
                    })
                    .state('app.subscription', {
                        url: '/subscription',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/subscription.html',
                                controller: 'SubscriptionController as subscribe'
                            }
                        }
                    })
                    .state('app.subscriptiondetails', {
                        url: '/subscriptiondetails/:subId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/subscriptiondetails.html',
                                controller: 'SubscriptionDetailsController as subsdetail'
                            }
                        }
                    })
                    .state('app.subscriptionpayment', {
                        url: '/subscriptionpayment',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/subscriptionpayment.html',
                                controller: 'SubscriptionPaymentController as subspay'
                            }
                        }
                    })
                    .state('app.login', {
                        url: '/login',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/login.html',
                                controller: 'LoginController as lc'
                            }
                        }
                    })
                    .state('app.myaccount', {
                        url: '/myaccount',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myaccount.html',
                                controller: 'MyAccountController as ma'
                            }
                        }
                    })
                    .state('app.notification', {
                        url: '/notification',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/notification.html',
                                controller: 'NotificationController as notice'
                            }
                        }
                    })
                    .state('app.mycloset', {
                        url: '/mycloset/:userId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/mycloset.html',
                                controller: 'MyClosetController as closet'
                            }
                        }
                    })
                    .state('app.mysetting', {
                        url: '/mysetting',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myaccountsetting.html',
                                controller: 'MyAccountSettingsController as ms'
                            }
                        }
                    })
                    .state('app.shareapp', {
                        url: '/shareapp',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/shareapp.html',
                                controller: 'ShareAppController as sa'
                            }
                        }
                    })
                    .state('app.registration', {
                        url: '/registration',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/registration.html',
                                controller: 'RegistrationController as rc'
                            }
                        }
                    })
                    .state('app.myaddress', {
                        url: '/myaddress',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myaddress.html',
                                controller: 'MyAddressController as address'
                            }
                        }
                    })
                    .state('app.feedback', {
                        url: '/myfeedback/:userId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myfeedback.html',
                                controller: 'MyFeedbackController as feed'
                            }
                        }
                    })
                    .state('app.inbox', {
                        url: '/inbox',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/inbox.html',
                                controller: 'InboxController as inbox'
                            }
                        }
                    })
                    .state('app.messagedetails', {
                        url: '/messagedetails/:threadId',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/messagedetails.html',
                                controller: 'MessageDetailsController as msg'
                            }
                        }
                    })
                    .state('app.customersupport', {
                        url: '/customersupport',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/customersupport.html',
                                controller: 'CustomerSupportController as support'
                            }
                        }
                    })
                    .state('app.tutorial', {
                        url: '/tutorial',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/tutorial.html',
                                controller: 'TutorialController as tutorial'
                            }
                        }
                    })
                    .state('app.solditem', {
                        url: '/solditem',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/solditem.html',
                                controller: 'SoldItemController as sold'
                            }
                        }
                    })
                    .state('app.myorder', {
                        url: '/myorder',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myorder.html',
                                controller: 'MyOrderController as order'
                            }
                        }
                    });
            // if none of the above states are matched, use this as the fallback
            if (window.localStorage['ngStorage-lastState']) {
                //$urlRouterProvider.otherwise('app' + window.localStorage['ngStorage-lastStateUrl'].slice(1, -1));
                //$urlRouterProvider.otherwise('/app/categoryall');
                $urlRouterProvider.otherwise('/app/livesalelist');
            } else {
                $urlRouterProvider.otherwise('/app/home');
            }
        })
        .factory('serverUrls', serverUrls);
serverUrls.$inject = [];
function serverUrls() {
    var HOSTURL = 'http://www.ibuysellapp.com/web-backend';
    var FORNTHOSTURL = 'http://www.ibuysellapp.com/webupload';
    var FIREBASELINK = 'https://ibuysellapp.firebaseio.com/';

    //var HOSTURL = 'http://98.191.125.87/dtswork/ibuysell';
    //var HOSTURL = 'http://localhost/ibuysellweb/index.php';
    var urls = {
        login: HOSTURL + '/Login/api_userloginprocess',
        getuserbyauthid: HOSTURL + '/Login/api_authuserlogin',
        insertuserbyauthid: HOSTURL + '/Login/api_authuser_registration',
        register: HOSTURL + '/Login/api_newuserregistration',
        updateprofile: HOSTURL + '/Login/api_updateuser',
        uploadprofileimage: HOSTURL + '/User/api_set_userpic',
        changepassword: HOSTURL + '/Login/api_changepassword',
        forgot: HOSTURL + '/Login/api_userforgot_password',
        getcategories: HOSTURL + '/Category/api_getcategories',
        //getshippingmethods: HOSTURL + '/Product/api_getshippingmethods',
        getshippingmethods: 'https://private-anon-300eab928-shipstation.apiary-mock.com/carriers',
        uploadproduct: HOSTURL + '/Product/api_uploadproduct',
        uploadproductimage: HOSTURL + '/Product/api_uploadproductimage',
        MessageCountNotification: HOSTURL + '/Notification/api_getallnotification_count',
        localCountNotification: HOSTURL + '/Notification/api_getall_local_notification_count',
        localNotification: HOSTURL + '/Notification/api_getall_local_notification',
        alertNotification: HOSTURL + '/Notification/api_alert_notification',
        getAllNotification: HOSTURL + '/Notification/api_getnotification',
        getAllClosets: HOSTURL + '/User/api_mycloset',
        getsettings: HOSTURL + '/Settings/api_getsettings',
        updateaddress: HOSTURL + '/User/api_updateaddress',
        getaddress: HOSTURL + '/User/api_getaddress',
        setpresetvalue: HOSTURL + '/Product/api_set_presetprice',
        removepresetvalue: HOSTURL + '/User/api_delete_user_preset_value',
        getpresetcount: HOSTURL + '/Product/api_get_tagcount',
        getsubscriptionbyid: HOSTURL + '/User/api_get_userplan',
        getsizebrand: HOSTURL + '/Category/api_getcategories_size',
        getsubscriptionplan: HOSTURL + '/Subscription/api_getplans',
        getsubscriptionplanbyid: HOSTURL + '/Subscription/api_getplanbyid',
        getrelatedproductbyid: HOSTURL + '/Product/api_get_related_product',
        updatesubscriptionplan: HOSTURL + '/Subscription/api_addsubscription_user',
        getseller: HOSTURL + '/Product/api_get_seller',
        gettaglistByuser: HOSTURL + '/User/api_get_taglist',
        insertcart: HOSTURL + '/Order/api_insert_cart',
        getcartcountbyuser: HOSTURL + '/Order/api_get_cartCount',
        getuserverify: HOSTURL + '/User/api_user_verified',
        getitemcondition: HOSTURL + '/Product/api_get_item_conditions',
        shareappapi: HOSTURL + '/User/api_share_app_link',
        submitrefferelcode: HOSTURL + '/User/api_set_refferal',
        getcms: HOSTURL + '/cms/api_get_content', 
        mainfaqlist: HOSTURL + '/Faq/api_faqCategory', 
        faqlist: HOSTURL + '/Faq/api_faqs',
        settrackcode: HOSTURL + '/Order/api_set_trackcode',
        getsolditem: HOSTURL + '/User/api_myorders',
        ///firebase table link
        firebaseLink: FIREBASELINK,
        firebaseCategory: FIREBASELINK + 'category',
        firebaseProduct: FIREBASELINK + 'products',
        firebaseLivesale: FIREBASELINK + 'livesale',
        cartDetails: HOSTURL + '/Order/api_get_cartContent',
        deleteCartItem: HOSTURL + '/Order/api_delete_cart',
        checkout: FORNTHOSTURL + '/checkout',
        cartTempSave: HOSTURL + '/Order/api_saveat_checkout',
        cartTempPaymentSave: HOSTURL + '/Order/api_update_checkout_payment_details',
        orderPaymentCheck: HOSTURL + '/Order/api_order_responce',
        updateProduct: HOSTURL + '/Product/api_update_product',
        getorderbyid: HOSTURL + '/Order/api_get_orders',
        setfeedbackrating: HOSTURL + '/Order/api_save_rating',
        getfeedback: HOSTURL + '/Order/api_my_feedback',
        setmessage: HOSTURL + '/User/api_set_message',
        getinboxmessage: HOSTURL + '/User/api_get_message',
        getmessagedetails: HOSTURL + '/User/api_get_message_details',
        verifycoupon: HOSTURL + '/Coupon/api_verify_coupon',
        applycoupon: HOSTURL + '/Coupon/api_apply_coupon',
        getshipstationcarriers: HOSTURL + '/Shipstation/api_get_carriers',
        getshipstationrates: HOSTURL + '/Shipstation/api_get_rates',
        bulkupload: FORNTHOSTURL + '/myaccount',
        printlabel: FORNTHOSTURL + '/myaccount/orderproducts',
        countrylist: HOSTURL + '/Staticdata/api_getAllowedCountry',
        accountpopdata: HOSTURL + '/Staticdata/api_getAccountPopupContain'
    };
    return urls;
}
