(function() {
    angular.module('ibuysell').controller('MyFeedbackController', MyFeedbackController);
    MyFeedbackController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage', '$stateParams','$q'];
    function MyFeedbackController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage, $stateParams,$q) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.feedback = {};
            if ($stateParams.userId)
            {
                var userId = $stateParams.userId;
            }
            else
            {
                var userId = $localStorage.loginData.user_ID;
            }
            var data = {seller_id: userId};
            var api1 = [];
            api1[0] =  UserService.getFeedbackById(data);
            api1[1] = UserService.getSellerById(userId);
            $q.all(api1).then(function(response) {
                $ionicLoading.hide();
                ib.feedback = response[0].data.data;
                if (userId == $localStorage.loginData.user_ID)
                {
                    ib.seller = "My";
                    ib.sellerFlag = true;
                }
                else
                {
                    ib.seller = response[1].data.data.username;
                    ib.sellerFlag = false;
                }
                ib.sellerDetails = response[1].data.data;
                ib.user_id=userId;
             });
        });

    }
})();
