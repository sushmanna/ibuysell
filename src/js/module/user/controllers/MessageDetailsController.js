(function() {
    angular.module('ibuysell').controller('MessageDetailsController', MessageDetailsController);
    MessageDetailsController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage', '$stateParams', '$q', '$ionicScrollDelegate'];
    function MessageDetailsController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage, $stateParams, $q, $ionicScrollDelegate) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.inboxmessage = {};
            var data = {user_id: $localStorage.loginData.user_ID, thread_id: $stateParams.threadId};
            ib.UserId = $localStorage.loginData.user_ID;
            UserService.getMessageById(data).success(function(response, status)
            {
                if (status == 200) {
                    ib.com_user_id = response.data.user_details.msg_by_user;
                    ib.inboxmessage = getmsg(response.data.msg_list);
                    $ionicScrollDelegate.scrollBottom();
                }
            });
        });

        ib.submitMessage = function()
        {
            if (ib.message != '')
            {
                $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                var process = [];
                var data = {posted_to: ib.com_user_id, posted_by: $localStorage.loginData.user_ID, msg_content: ib.message, msg_type: 'message', msg_thread: $stateParams.threadId};
                process[0] = UserService.sendMessageOrSupport(data);
                var data1 = {user_id: $localStorage.loginData.user_ID, thread_id: $stateParams.threadId};
                process[1] = UserService.getMessageById(data1);
                $q.all(process)
                        .then(function(result) {
                            ib.message = '';
                            ib.inboxmessage = getmsg(result[1].data.data.msg_list);
                        })
                        .then(function() {
                            $ionicLoading.hide();
                        });
            }
            else
            {
                ionicToast.show('Please enter text.', 'middle', false, 2500);
            }

        };
        function getmsg(resultSet) {
            var m = [];
            for (k in resultSet)
            {
                element = {};
                if (resultSet[k].msg_by_user == ib.UserId)
                {
                    element.mymeaage = resultSet[k].msg_content;
                    element.mymeaageTime = resultSet[k].msg_posted;
                    element.usermeaage = '';
                    element.usermeaageTime = '';
                    element.msg_by_user = resultSet[k].msg_by_user

                }
                else
                {
                    element.mymeaage = '';
                    element.mymeaageTime = '';
                    element.usermeaage = resultSet[k].msg_content;
                    element.usermeaageTime = resultSet[k].msg_posted;
                    element.msg_by_user = resultSet[k].msg_by_user
                }
                m.push(element);
            }
            return m;
        }
    }
})();
