(function() {
    angular.module('ibuysell').controller('MyAccountSettingsController', MyAccountSettingsController);
    MyAccountSettingsController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', 'UserService', '$ionicLoading', 'ionicToast', '$ionicScrollDelegate'];
    function MyAccountSettingsController($scope, $rootScope, $ionicModal, $timeout, $localStorage, UserService, $ionicLoading, ionicToast, $ionicScrollDelegate) {
        var ib = this;

        $ionicModal.fromTemplateUrl('js/module/user/templates/passwordModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.passwordModal = modal;
        });
        var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

        ib.datepickerObject = {
            titleLabel: 'Choose Date of Birth',  
            todayLabel: 'Today',  
            closeLabel: 'Close',  
            setLabel: 'Set',  
            setButtonType : 'button-positive',  
            todayButtonType : 'button-positive',  
            closeButtonType : 'button-positive',  
            inputDate: new Date(),  
            mondayFirst: true,  
            //disabledDates: disabledDates, 
            //weekDaysList: weekDaysList, 
            monthList: monthList, 
            templateType: 'popup', 
            showTodayButton: 'true', 
            modalHeaderColor: 'bar-positive', 
            modalFooterColor: 'bar-positive', 
            //from: new Date(2012, 8, 2), 
            //to: new Date(2018, 8, 25),  
            callback: function (val) {  
                ib.datePickerCallback(val);
            },
            dateFormat: 'dd-MM-yyyy', 
            closeOnSelect: false
        };

        ib.datePickerCallback = function (val) {
            if (typeof(val) !== 'undefined') {
                var dateToCompare = new Date(val);
                var currentDate = new Date();
                if (dateToCompare < currentDate) {
                    ib.userdata.dob = val.getFullYear()+'-'+(val.getMonth()+1)+'-'+val.getDate();
                }
            }
        };

        $scope.$on('$ionicView.enter', function() {
            // Code you want executed every time view is opened
            UserService.getSellerById($localStorage.loginData.user_ID).success(function(response, status)
            {
                if (status == 200) {
                    ib.userdata=response.data;
                    ib.userdata.user_ID=$localStorage.loginData.user_ID;
                    var dd = ib.userdata.dob;
                    /*if (ib.userdata.dob)
                    {
                        ib.userdata.dob = new Date(dd);
                    } else
                    {
                        ib.userdata.dob =  '';
                    }*/
                }
            });
            $ionicScrollDelegate.scrollTop(true);
        });

        ib.updateProfile = function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            UserService.updateProfile(ib.userdata).success(function(response, status)
            {
                $ionicLoading.hide();
                if (status == 200) {
                    $localStorage.loginData = response.data;
                    $rootScope.loginData = response.data;
                    ionicToast.show(response.msg, 'top', false, 2500);
                } else
                {
                    ionicToast.show(response.msg, 'top', false, 2500);
                }
            });
        }

        ib.closeChangePasswordModal = function() {
            ib.passwordModal.hide();
        };
        //open modal for forgot password
        ib.openChangePasswordModal = function() {
            ib.successmsg = '';
            ib.errormsg = '';
            ib.passdata = [];
            ib.passwordModal.show();
        };

        ib.changePassword = function() {
            ib.passdata.emailid = ib.userdata.emailid;
            UserService.changePassword(ib.passdata).success(function(response, status)
            {
                if (status == 200) {
                    ionicToast.show(response.msg, 'middle', false, 2500);
                    ib.passwordModal.hide();
                } else
                {
                    ib.passdata = [];
                    ib.errormsg = response.msg;
                }
            });
        };
    }
})();