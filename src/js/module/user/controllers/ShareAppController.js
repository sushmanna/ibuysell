(function() {
    angular.module('ibuysell').controller('ShareAppController', ShareAppController);
    ShareAppController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$cordovaSocialSharing', 'ionicToast', '$localStorage', 'UserService'];
    function ShareAppController($scope, $rootScope, $ionicModal, $timeout, $cordovaSocialSharing, ionicToast, $localStorage, UserService) {
        var ib = this;
        $scope.$on('$ionicView.enter', function() {
            ib.logindata = $localStorage.loginData;
            var data = {user_id: $localStorage.loginData.user_ID}
            UserService.shareAppApi(data).success(function(response, status)
            {
                if (status == 200) {
                    console.log(response);
                    ib.Messaage = response.data.message;
                    ib.shareCode = response.data.share_code;
                    ib.accountBalance = response.data.account_balance;
                    ib.referalflag = response.data.reffer_status;
                    ib.referalCode = response.data.reffered_with_code;
                    ib.accountsummary = response.data.account_statement;
//                    ib.accountsummary = [
//                        {
//                            as_statement:"Referral Buyer",
//                            as_state_date: 1450343209,
//                            as_amount: 12
//                        },
//                       {
//                            as_statement:"Referral Buyer",
//                            as_state_date: 1450343209,
//                            as_amount: -12
//                        },
//                       {
//                            as_statement:"Referral Buyer",
//                            as_state_date: 1450343209,
//                            as_amount: -12
//                        }, 
//                        {
//                            as_statement:"Referral Buyer",
//                            as_state_date: 1450343209,
//                            as_amount: 12
//                        },
//                        {
//                            as_statement:"Referral Buyer",
//                            as_state_date: 1450343209,
//                            as_amount: -12
//                        },
//                    ];
                }
            });
        });
        $ionicModal.fromTemplateUrl('js/module/user/templates/shareMsgModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.shareModal = modal;
        });
        //close email modal
        ib.closeShareModal = function() {
            ib.shareModal.hide();
        };
        //open modal for forgot password
        ib.openShareModal = function() {
            ib.shareMessaage = $scope.master = angular.copy(ib.Messaage);
            ib.shareModal.show();
        };
        $ionicModal.fromTemplateUrl('js/module/user/templates/creditModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.creditModal = modal;
        });
        //close email modal
        ib.closeCreditModal = function() {
            ib.creditModal.hide();
        };
        //open modal for forgot password
        ib.openCreditModal = function() {
            ib.creditModal.show();
        };
        ib.toggleReferral = function() {
            var s = $('#referral-div');
            s.slideToggle();
        };
        ib.submitReferralCode = function() {
            var data = {user_id: $localStorage.loginData.user_ID, reffer_code: ib.referral_code};
            UserService.updateReferel(data).success(function(response, status)
            {
                if (status == 200) {
                    ib.referalflag = true;
                    ib.referalCode = ib.referral_code;
                    ib.toggleReferral();
                    ionicToast.show('Referel code save successfully.', 'middle', false, 2500);
                }
                else
                {
                    ionicToast.show('Error:' + response.msg, 'middle', false, 2500);
                }
            });
        };
        ib.shareApp = function()
        {
            ib.shareModal.hide();
            $cordovaSocialSharing
                    .share(ib.shareMessaage, null, null)
                    .then(function(result) {
                        //ionicToast.show('You have successfully share the message', 'middle', true, 2500);
                    }, function(err) {
                        // An error occurred. Show a message to the user
                        ionicToast.show('Error:' + err, 'middle', false, 2500);
                    });
        };
    }
})();
