(function(){
    angular.module('ibuysell').controller('LoginController', LoginController);
    LoginController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$firebaseAuth', '$ionicModal', '$ionicPlatform','$ionicHistory','$ionicPopup', '$localStorage', 'NotificationService','serverUrls'];
    function LoginController($scope, $rootScope, $ionicModal, $timeout, $location, UserService, ionicToast, $ionicLoading, $firebaseAuth, $ionicModal, $ionicPlatform ,$ionicHistory, $ionicPopup, $localStorage, NotificationService,serverUrls){
        var ib = this;
        ib.logindata = {};
        //ib.logindata.emailid = 'codetestjohny@gmail.com';
        //ib.logindata.password = '123456';
        var ref = new Firebase(serverUrls.firebaseLink);
        $ionicModal.fromTemplateUrl('js/module/user/templates/emailModal.html', {
            scope: $scope
        }).then(function(modal){
            ib.loginModal = modal;
        });
        $scope.$on('$ionicView.beforeEnter', function(){
            if( $localStorage.loginData){
                //$location.path("/app/categoryall");
                $location.path("/app/livesalelist");
                return;
            }
            ib.logindata = {emailid : '', password : ''};
            ib.emailheader = 'Enter Email';
        });
        //normal login
        ib.login = function(){
            if( $localStorage.device_details){
                ib.logindata.device_type = $localStorage.device_details.device_type;
                ib.logindata.device_token = $localStorage.device_details.device_token;
                ib.logindata.login_frm = 'device';
            }

            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            UserService.login(ib.logindata).success(function(response, status)
            {
                $ionicLoading.hide();
                if(status == 200){
                    $localStorage.loginData = response.data;
                    $rootScope.loginData = response.data;
                    NotificationService.getMessageNotifications($rootScope.loginData.user_ID);
                    NotificationService.getLocalNotifications($rootScope.loginData.user_ID);
                    NotificationService.getAlertNotifications($rootScope.loginData.user_ID);
                    //$location.path("/app/categoryall");
                    $location.path("/app/livesalelist");
                }
                else
                {
                    ionicToast.show(response.msg, 'top', false, 2500);
                }
            });
        };
        //auth login
        ib.authLogin = function(loginType){
            if( $localStorage.device_details){
                ib.logindata.device_type = $localStorage.device_details.device_type;
                ib.logindata.device_token = $localStorage.device_details.device_token;
                ib.logindata.login_frm = 'device';
            }
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            var auth = $firebaseAuth(ref);
            auth.$authWithOAuthPopup(loginType,{ remember: "sessionOnly", scope: "email"})
            //auth.$authWithOAuthPopup(loginType)
                    .then(function(authData){
                        $ionicLoading.hide();
                        ib.logindata.provider = loginType;
                        if(loginType == 'facebook'){
                            ib.logindata.fname = authData.facebook.displayName;
                            ib.logindata.authId = authData.facebook.id;
                            ib.logindata.user_img = authData.facebook.profileImageURL;
                            if( authData.facebook.email) ib.logindata.emailid = authData.facebook.email;
                            ib.emailheader = 'Enter Facebook Email';
                        }
                        else if(loginType == 'twitter')
                        {
                            ib.logindata.fname = authData.twitter.displayName;
                            ib.logindata.authId = authData.twitter.id;
                            ib.logindata.user_img = authData.twitter.profileImageURL;
                            if( authData.twitter.email) ib.logindata.emailid = authData.twitter.email;
                            ib.emailheader = 'Enter Twitter Email';
                        }
                        
                        UserService.checkUser(ib.logindata).success(function(response, status)
                        {
                            if(status == 200){
                                $localStorage.loginData = response.data;
                                $rootScope.loginData = response.data;
                                NotificationService.getMessageNotifications($rootScope.loginData.user_ID);
                                NotificationService.getLocalNotifications($rootScope.loginData.user_ID);
                                NotificationService.getAlertNotifications($rootScope.loginData.user_ID);
                                //$location.path("/app/categoryall");
                                $location.path("/app/livesalelist");
                            }else
                            {
                                ib.action = 'login';
                                ib.errormsg = '';
                                if( ib.logindata.emailid){
                                    ib.doLogin( ib.action);
                                }
                                else{
                                    ib.logindata.emailid = '';
                                    ib.loginModal.show();
                                }
                            }
                        });
                    }, function(error){
                        ionicToast.show(error, 'top', false, 2500);
                        $ionicLoading.hide();
                    })
                    .catch(function(error){
                        ionicToast.show("Authentication failed:".error, 'top', false, 2500);
                    });
        };
        //close email modal
        ib.closeLogin = function(){
            ib.loginModal.hide();
        };
        //open modal for forgot password
        ib.openModal = function(){
            ib.action = 'forgot';
            ib.emailheader = 'Enter Account Email';
            ib.logindata.emailid = '';
            ib.errormsg = '';
            ib.loginModal.show();
        };
        //register and login
        ib.doLogin = function(action){
            if(action == 'login'){
                //console.log(ib.logindata);
                UserService.authRegister(ib.logindata).success(function(response, status)
                {
                    if(status == 200){
                        $localStorage.loginData = response.data;
                        $rootScope.loginData = response.data;
                        ib.loginModal.hide();
                        NotificationService.getMessageNotifications($rootScope.loginData.user_ID);
                        NotificationService.getLocalNotifications($rootScope.loginData.user_ID);
                        NotificationService.getAlertNotifications($rootScope.loginData.user_ID);
                        //$location.path("/app/categoryall");
                        $location.path("/app/livesalelist");
                    }else
                    {
                        ib.closeLogin();
                        ionicToast.show( response.msg, 'top', false, 2500);
                        //ib.errormsg = response.msg;
                    }
                });
            }else if(action == 'forgot'){
                UserService.forgotpassword(ib.logindata).success(function(response, status)
                {
                    if(status == 200){
                        ionicToast.show(response.msg, 'top', false, 2500);
                        ib.loginModal.hide();
                        $location.path("/app/login");
                    }else
                    {
                        ib.errormsg = response.msg;
                    }
                });
            }
        };
    }
})();
