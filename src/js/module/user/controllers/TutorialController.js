(function() {
    angular.module('ibuysell').controller('TutorialController', TutorialController);
    TutorialController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage', '$ionicSlideBoxDelegate'];
    function TutorialController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage, $ionicSlideBoxDelegate) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.slides = ['img/banner02.jpg', 'img/banner01.jpg', 'img/banner03.jpg'];
        });
        $scope.$on('$ionicView.afterEnter', function() {
            ib.changeslide();
        });
        ib.changeslide = function()
        {
            $ionicSlideBoxDelegate.next();
        };
    }
})();
