(function() {
    angular.module('ibuysell').controller('SubscriptionController', SubscriptionController);
    SubscriptionController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', 'UserService', 'DataService', '$ionicLoading', 'ionicToast', '$location', '$q', '$ionicPopup', '$cordovaInAppBrowser', 'serverUrls'];

    function SubscriptionController($scope, $rootScope, $ionicModal, $timeout, $localStorage, UserService, DataService, $ionicLoading, ionicToast, $location, $q, $ionicPopup, $cordovaInAppBrowser, serverUrls) {
        var ib = this;
        ib.groupClick = function(plan) {
            $location.path('/app/subscriptiondetails/'+plan.subscription_ID);
        };
        
        
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            DataService.setData({});
            ib.userdata = $localStorage.loginData;
            ib.plans = [];
            ib.payperplans = [];
            ib.subscribeData = { subscription_id: '1-0', subscription_term: 0};
            var process = [];
            //get getSubscriptionPlan 
            var data = {};
            process[0] = UserService.getSubscriptionPlan();
            process[1] = UserService.getSubscriptionById(ib.userdata.user_ID);
            process[2] = UserService.getAllAddress({
                user_id: ib.userdata.user_ID
            });
            $q.all(process).then(function(result) {
                ib.plans = result[0].data.data.subscriptions;
                ib.subscribeData = result[1].data.data;

                if( ib.subscribeData.subscription_id && !ib.subscribeData.user_sub_transection_ID){
                    ib.subscribeData.user_sub_transection_ID = 'pay_per_transaction';
                }

                if( ib.subscribeData.subscription_id){
                    ib.alreadySubscribe = 1;
                    ib.prePlanText = '';
                    angular.forEach(ib.plans, function(value, key){
                        if( value.subscription_ID == ib.subscribeData.subscription_id){
                            ib.prePlanText = ib.prePlanText + value.subscription_title;
                            if( ib.subscribeData.subscription_term == 1) ib.prePlanText = ib.prePlanText + ' @ $'+value.plans.monthly.per_month_price+'/mo.';
                            if( ib.subscribeData.subscription_term == 6) ib.prePlanText = ib.prePlanText + ' @ $'+value.plans.semiannual.per_month_price+'/mo.';   
                            if( ib.subscribeData.subscription_term == 12) ib.prePlanText = ib.prePlanText + ' @ $'+value.plans.annual.per_month_price+'/mo.';   
                        }
                    });
                }

                ib.subscribeData.userdata = result[2].data.data;
                ib.subscribeData.subscriptionDetails = result[0].data.data.subscription_details;
                $ionicLoading.hide();
            });
        });

        $ionicModal.fromTemplateUrl('js/module/user/templates/subscriptionmodal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.subscribeModal = modal;
        });
        ib.closeSubscribeModal = function() {
            ib.subscribeModal.hide();
            ib.details = {};
        };
        ib.openSubscribeModal = function(validity, plan, plandetails) {
            ib.details = {};
            if (plan.subscription_type == 'pay_per_transaction') {
                ib.details.planname = plan.subscription_title;
                ib.details.price = plan.subscription_price;
                ib.details.total_price = 0;
                ib.details.validity = 'Per Transaction';
                ib.details.description = plan.subscription_description;
                ib.details.plan_type = plan.subscription_type;
                ib.details.limit = 'Not Applicable';
                ib.btnLabel = 'FINISH';
                ib.subscribeData.subscription_term = 0;
            } else {
                ib.details.planname = plan.subscription_title;
                ib.details.price = plandetails.per_month_price;
                ib.details.total_price = plandetails.total_price;
                ib.details.validity = plandetails.display_name;
                ib.details.description = plan.subscription_description;
                ib.details.plan_type = plan.subscription_type;
                ib.details.limit = !(parseInt(plan.subscription_sell_limit)) ? 'Unlimited' : plan.subscription_sell_limit;
                ib.btnLabel = 'PAY NOW';
                ib.subscribeData.subscription_term = plandetails.term;
            }
            ib.subscribeModal.show();
        };
        
        
        ib.cancelSubscription = function() {
            $ionicPopup.confirm({
                title: 'Are you sure?',
                subTitle: 'Please confirm.',
                template: 'You want to cancel your current subscription plan.',
                okText: "Yes",
                cancelText: "No"
            }).then(function(res) {
                if (res) {
                    $ionicLoading.show({
                        template: '<img src="img/loader.GIF">'
                    });
                    var options = {
                        location: 'no',
                        clearcache: 'yes',
                        toolbar: 'no'
                    };
                    var callexit = false;
                    
                    var urlparam = 'user_id=' + ib.userdata.user_ID + '&subscription_id=' + ib.subscribeData.subscription_id + '&paypal_transaction_id=' + ib.subscribeData.user_sub_transection_ID;
                    $cordovaInAppBrowser.open(serverUrls.checkout + '/cancelsubscription?'+urlparam, '_blank', options).then(function(event) {
                        console.log('Success');
                    }).catch(function(event) {
                        console.log('Error');
                    });
                    $scope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
                        if (event.url.match("checkout/close")) {
                            $cordovaInAppBrowser.close();
                        }
                    });
                    $scope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                        // Call the external API
                        if (!callexit) {
                            callexit = true;
                            $ionicLoading.hide();
                            $timeout(function() {
                                DataService.setData({});
                                $location.path("/app/myaccount");
                            }, 3000);
                        }
                    });
                } else {
                    console.log('failure');
                }
            });
        };
    }
})();