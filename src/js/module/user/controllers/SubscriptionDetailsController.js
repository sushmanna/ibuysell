(function() {
    angular.module('ibuysell').controller('SubscriptionDetailsController', SubscriptionDetailsController);
    SubscriptionDetailsController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', 'UserService', 'DataService', '$ionicLoading', 'ionicToast', '$location', '$q', '$ionicPopup', '$cordovaInAppBrowser', '$stateParams', 'serverUrls'];

    function SubscriptionDetailsController($scope, $rootScope, $ionicModal, $timeout, $localStorage, UserService, DataService, $ionicLoading, ionicToast, $location, $q, $ionicPopup, $cordovaInAppBrowser, $stateParams, serverUrls) {
        var ib = this;


        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            DataService.setData({});
            ib.userdata = $localStorage.loginData;
            ib.btnLabel = 'PAY NOW';
            ib.plans = [];
            ib.payperplans = [];
            ib.subscribeData = { subscription_id: '1-0', subscription_term: 0};
            var process = [];
            //get getSubscriptionPlan 
            var data = {};
            ib.countrylist = {};
            process[0] = UserService.getSubscriptionPlanById({ subscription_ID: $stateParams.subId});
            process[1] = UserService.getSubscriptionById(ib.userdata.user_ID);
            process[2] = UserService.getAllAddress({
                user_id: ib.userdata.user_ID
            });
            process[3] = UserService.countrylist();
            $q.all(process).then(function(result) {
            	ib.plans = result[0].data.data;
                ib.subscribeData = result[1].data.data;

                if( ib.subscribeData.user_sub_transection_ID && ib.subscribeData.user_sub_transection_ID == 'null'){
                    ib.subscribeData.user_sub_transection_ID = 'pay_per_transaction';
                }

                if( ib.subscribeData.subscription_id){ 
                    ib.alreadySubscribe = 1;
                    ib.pre_subscription_id = ib.subscribeData.subscription_id+'-'+ib.subscribeData.subscription_term;
                    ib.subscribeData.subscription_only_id = angular.copy(ib.subscribeData.subscription_id);
                    ib.subscribeData.subscription_id = ib.subscribeData.subscription_id+'-'+ib.subscribeData.subscription_term;
                }
                if( !ib.subscribeData.subscription_id) { ib.alreadySubscribe = 0; ib.subscribeData.subscription_only_id=1; ib.subscribeData.subscription_id = '1-0'; ib.btnLabel = 'FINISH';}
                if( !ib.subscribeData.subscription_term) { ib.subscribeData.subscription_term = 0;}
                
                ib.subscribeData.verifyCoupon = false;
                ib.subscribeData.takePaymentCoupon = false;
                ib.subscribeData.userdata = result[2].data.data;
                ib.subscribeData.subscriptionDetails = result[0].data.data.subscription_details;
                ib.countrylist = result[3].data.data.country;
                $ionicLoading.hide();
            });
        });
		
		$ionicModal.fromTemplateUrl('js/module/user/templates/subscriptionmodal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.subscribeModal = modal;
        });

		ib.closeSubscribeModal = function() {
            ib.subscribeModal.hide();
            ib.details = {};
        };
        ib.openSubscribeModal = function(validity, plan, plandetails) {
            ib.details = {};
            if (plan.subscription_type == 'pay_per_transaction') {
                ib.details.planname = plan.subscription_title;
                ib.details.price = plan.subscription_price;
                ib.details.total_price = 0;
                ib.details.validity = 'Per Transaction';
                ib.details.description = plan.subscription_description;
                ib.details.plan_type = plan.subscription_type;
                ib.details.limit = 'Not Applicable';
                ib.btnLabel = 'FINISH';
                ib.subscribeData.subscription_term = 0;
            } else {
                ib.details.planname = plan.subscription_title;
                ib.details.price = plandetails.per_month_price;
                ib.details.total_price = plandetails.total_price;
                ib.details.validity = plandetails.display_name;
                //ib.details.description = plan.subscription_description;
                ib.details.description = plandetails.description;
                ib.details.plan_type = plan.subscription_type;
                ib.details.limit = !(parseInt(plan.subscription_sell_limit)) ? 'Unlimited' : plan.subscription_sell_limit;
                ib.btnLabel = 'PAY NOW';
                ib.subscribeData.subscription_term = plandetails.term;
            }
            ib.subscribeModal.show();
        };

		ib.toggleAddress = function(divid) {
            console.log('Toggle Address');
            if (divid == 'shipping') ib.shippingcls = !ib.shippingcls;
            if (divid == 'billing') ib.billingcls = !ib.billingcls;
            var s = $('#' + divid);
            s.slideToggle();
        };

		ib.updateSubscription = function() {
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var validCheck = true;
            if (!ib.subscribeData.subscription_id) {
                validCheck = false;
                ionicToast.show('Please select subscription.', 'top', false, 2500);
            } else if ( !ib.subscribeData.account_related_email) {
                validCheck = false;
                ionicToast.show('Missing Paypal Email', 'top', false, 2500);
            } else if(!ib.subscribeData.account_related_email.match(mailformat)) {
                validCheck = false;
                ionicToast.show('Please Enter valid Email', 'top', false, 2500);
            } else if ( ib.subscribeData.userdata.s_name === null || ib.subscribeData.userdata.s_name.length == 0) {
                validCheck = false;
                ionicToast.show('Missing shipping full name.', 'top', false, 2500);
            } else if ( ib.subscribeData.userdata.s_address1 === null || ib.subscribeData.userdata.s_address1.length == 0) {
                validCheck = false;
                ionicToast.show('Missing shipping address 1.', 'top', false, 2500);
            } else if ( ib.subscribeData.userdata.s_city === null || ib.subscribeData.userdata.s_city.length == 0) {
                validCheck = false;
                ionicToast.show('Missing shipping city.', 'top', false, 2500);
            } else if ( ib.subscribeData.userdata.s_state === null || ib.subscribeData.userdata.s_state.length == 0) {
                validCheck = false;
                ionicToast.show('Missing shipping state.', 'top', false, 2500);
            } else if ( ib.subscribeData.userdata.s_country === null || ib.subscribeData.userdata.s_country.length == 0) {
                validCheck = false;
                ionicToast.show('Missing shipping country.', 'top', false, 2500);
            } else if ( ib.subscribeData.userdata.s_zip === null || ib.subscribeData.userdata.s_zip.length == 0) {
                validCheck = false;
                ionicToast.show('Missing shipping zip.', 'top', false, 2500);
            } else if (!ib.subscribeData.userdata.checkBoxValue && (ib.subscribeData.userdata.b_name === null || ib.subscribeData.userdata.b_name.length == 0)) {
                validCheck = false;
                ionicToast.show('Missing billing full name.', 'top', false, 2500);
            } else if (!ib.subscribeData.userdata.checkBoxValue && (ib.subscribeData.userdata.b_address1 === null || ib.subscribeData.userdata.b_address1.length == 0)) {
                validCheck = false;
                ionicToast.show('Missing billing address 1.', 'top', false, 2500);
            } else if (!ib.subscribeData.userdata.checkBoxValue && (ib.subscribeData.userdata.b_city === null || ib.subscribeData.userdata.b_city.length == 0)) {
                validCheck = false;
                ionicToast.show('Missing billing city.', 'top', false, 2500);
            } else if (!ib.subscribeData.userdata.checkBoxValue && (ib.subscribeData.userdata.b_state === null || ib.subscribeData.userdata.b_state.length == 0)) {
                validCheck = false;
                ionicToast.show('Missing billing state.', 'top', false, 2500);
            } else if (!ib.subscribeData.userdata.checkBoxValue && (ib.subscribeData.userdata.b_country === null || ib.subscribeData.userdata.b_country.length == 0)) {
                validCheck = false;
                ionicToast.show('Missing billing country.', 'top', false, 2500);
            } else if (!ib.subscribeData.userdata.checkBoxValue && (ib.subscribeData.userdata.b_zip === null || ib.subscribeData.userdata.b_zip.length == 0)) {
                validCheck = false;
                ionicToast.show('Missing billing zip.', 'top', false, 2500);
            } else if ( !ib.subscribeData.verifyCoupon && ib.subscribeData.coupon_code){
                validCheck = false;
                ionicToast.show('Verify your coupon.', 'top', false, 2500);
            } else if ( ib.alreadySubscribe && ib.pre_subscription_id == ib.subscribeData.subscription_id){
                validCheck = false;
            }

            if (validCheck) {
                if (ib.subscribeData.userdata.checkBoxValue) {
                    ib.subscribeData.userdata.b_name = ib.subscribeData.userdata.s_name;
                    ib.subscribeData.userdata.b_address1 = ib.subscribeData.userdata.s_address1;
                    ib.subscribeData.userdata.b_address2 = ib.subscribeData.userdata.s_address2;
                    ib.subscribeData.userdata.b_city = ib.subscribeData.userdata.s_city;
                    ib.subscribeData.userdata.b_state = ib.subscribeData.userdata.s_state;
                    ib.subscribeData.userdata.b_country = ib.subscribeData.userdata.s_country;
                    ib.subscribeData.userdata.b_zip = ib.subscribeData.userdata.s_zip;
                    ib.subscribeData.userdata.b_phone = ib.subscribeData.userdata.s_phone;
                }
                $ionicLoading.show({
                    template: '<img src="img/loader.GIF">'
                });
                var subsId = ib.subscribeData.subscription_id.split('-');
                ib.subscribeData.subscription_id = subsId[0];
                ib.subscribeData.subscription_term = subsId[1];
                ib.subscribeData.user_id = ib.userdata.user_ID;
                ib.subscribeData.userdata.user_id = ib.userdata.user_ID;
                if (ib.subscribeData.verifyCoupon) {
                    UserService.applyCoupon({
                        coupon_code: ib.subscribeData.coupon_code,
                        user_id: ib.userdata.user_ID
                    }).success(function(response, status) {
                        $ionicLoading.hide();
                        ionicToast.show(response.msg, 'top', false, 500);
                        if (status == 200) {
                            ib.processSubscription();
                            ib.subscribeData.subscription_id = ib.subscribeData.coupon_subscription_id;
                            ib.subscribeData.subscription_term = ib.subscribeData.coupon_subscription_term;
                        }
                    });
                } else if (!ib.subscribeData.coupon_code) {
                    $ionicLoading.hide();
                    ib.processSubscription();
                }
            }
        };
        ib.processSubscription = function() {
            $ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            var process = [];
            process.push(UserService.updateAddress(ib.subscribeData.userdata));
            process.push(UserService.updateSubscriptionById(ib.subscribeData));
            $q.all(process).then(function(result) {
                $ionicLoading.hide();
                ionicToast.show( result[1].data.msg, 'top', false, 2500);
                if( result[1].status == 200){
                    if( ib.subscribeData.verifyCoupon){
                        if (ib.subscribeData.takePaymentCoupon) { $location.path("/app/subscriptionpayment"); DataService.setData({ ch_token: result[1].data.data.ch_token, subscription_id: ib.subscribeData.subscription_id, subscription_term:ib.subscribeData.subscription_term});}
                        else $location.path("/app/categoryall");
                    } else if ( !ib.subscribeData.coupon_code){
                        if( !parseInt(ib.subscribeData.subscription_term)) $location.path("/app/categoryall");
                        else { $location.path("/app/subscriptionpayment");  DataService.setData({ ch_token: result[1].data.data.ch_token, subscription_id: ib.subscribeData.subscription_id, subscription_term:ib.subscribeData.subscription_term}); }
                    }
                }
            });
        };

		ib.toggleCoupon = function() {
            var s = $('#coupon-div');
            s.slideToggle();
        };
        ib.verifyCoupon = function() {
            $ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            var data = {
                coupon_code: ib.subscribeData.coupon_code,
                user_id: ib.userdata.user_ID
            };
            UserService.verifyCoupon(data).success(function(response, status) {
                $ionicLoading.hide();
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', false, 2500);
                    if (response.data) {
                        ib.btnLabel = response.data.buttonText;
                        ib.subscribeData.verifyCoupon = true;
                        ib.subscribeData.takePaymentCoupon = response.data.takePayment;
                        ib.subscribeData.coupon_subscription_id = response.data.subscription_id;
                        ib.subscribeData.coupon_subscription_term = response.data.subscription_term;
                    }
                }
            });
        };
    }
})();