(function() {
    angular.module('ibuysell').controller('MyOrderController', MyOrderController);
    MyOrderController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$cordovaSocialSharing', 'ionicToast', 'UserService', '$localStorage', '$ionicLoading', '$ionicPopup', '$ionicLoading','$ionicScrollDelegate'];

    function MyOrderController($scope, $rootScope, $ionicModal, $timeout, $cordovaSocialSharing, ionicToast, UserService, $localStorage, $ionicLoading, $ionicPopup, $ionicLoading, $ionicScrollDelegate) {
        var ib = this;
        /*set rating modal*/
        $ionicModal.fromTemplateUrl('js/module/user/templates/ratingModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.ratingModal = modal;
        });
        //close messageModal modal
        ib.closeRatingModal = function() {
            ib.ratingModal.hide();
        };
        //open modal for messageModal
        ib.openRatingModal = function(index) {
            ib.sellerDetails = {};
            ib.message = '';
            ib.index = index;
            ib.sellerDetails = ib.myOrder[index];
            if (ib.sellerDetails.order_feedback_given == 'no') {
                ib.ratingModal.show();
            } else {
                ionicToast.show('You have already rate this seller.', 'top', false, 2500);
            }
        };
        //send messageModal
        ib.submitRatingModal = function() {
            var data = {
                rate_by_buyer: $localStorage.loginData.user_ID,
                rate_to_seller: ib.sellerDetails.order_seller_id,
                rate_to_product: ib.sellerDetails.order_item_ids,
                rate_comment: ib.message,
                rate_given: ib.rate
            };
            UserService.setRatingByProductId(data).success(function(response, status) {
                if (status == 200) {
                    ib.myOrder[ib.index].order_feedback_given = 'yes';
                    ib.sellerDetails = {};
                    ib.message = '';
                    ib.rate = 0;
                    ib.ratingModal.hide();
                }
            });
        };
        $scope.$on('$ionicView.enter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.myOrder = [];
            ib.rate = 5;
            ib.max = 5;
            UserService.getOrderById($localStorage.loginData.user_ID).success(function(response, status) {
                if (status == 200) {
                    ib.myOrder = response.data;
                    $ionicScrollDelegate.scrollTop();
                }
                $ionicLoading.hide();
            });
        });
        ib.ratingsObject = {
            iconOn: 'ion-ios-star', //Optional
            iconOff: 'ion-ios-star-outline', //Optional
            iconOnColor: 'red', //Optional
            iconOffColor: 'black', //Optional
            rating: 2, //Optional
            minRating: 1, //Optional
            readOnly: true, //Optional
            callback: function(rating) { //Mandatory
                ib.ratingsCallback(rating);
            }
        };
        ib.ratingsCallback = function(rating) {
            console.log('Selected rating is : ', rating);
        };
        ib.getshipcode = function(code) {
            if (code != '' && code) {
                var templa = '<h3 class="subs-sub-title coupon-code-btn">Tracking code of your product is  :' + code + '</h3>';
            } else {
                var templa = '<h3 class="subs-sub-title coupon-code-btn">Seller has not provided Tracking code yet.</h3>';
            }
            var alertPopup = $ionicPopup.alert({
                title: 'Tracking Code',
                template: templa
            });
            alertPopup.then(function(res) {
                console.log('Thank you for not eating my delicious ice cream cone');
            });
        };
    }
})();