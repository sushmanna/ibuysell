(function() {
    angular.module('ibuysell').controller('MyClosetController', MyClosetController);
    MyClosetController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage', '$stateParams', '$q'];
    function MyClosetController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage, $stateParams, $q) {
        var ib = this;
        ib.myclosets = {};
        $scope.$on('$ionicView.enter', function() {
             $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.myclosets = {};
            if ($stateParams.userId)
            {
                var userId = $stateParams.userId;
            }
            else
            {
                var userId = $localStorage.loginData.user_ID;
            }
            var data = {user_id: userId};
            var process = [];
            process[0] = UserService.getAllClosets(data);
            process[1] = UserService.getSellerById(userId);
            $q.all(process).then(function(result) {
                if (userId == $localStorage.loginData.user_ID)
                {
                    ib.seller = "My";
                    ib.sellerFlag = true;
                }
                else
                {
                    ib.seller = result[1].data.data.u_username;
                    ib.sellerFlag = false;
                }
                ib.sellerDetails = result[1].data.data;
                ib.sellerDetails.user_id=userId;
                ib.closets = result[0].data.data;
                $ionicLoading.hide();
            });
        });

    }
})();
