(function() {
    angular.module('ibuysell').controller('SoldItemController', SoldItemController);
    SoldItemController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', 'ionicToast', 'UserService', '$localStorage', '$ionicLoading', '$ionicPopup'];
    function SoldItemController($scope, $rootScope, $ionicModal, $timeout, ionicToast, UserService, $localStorage, $ionicLoading, $ionicPopup) {
        var ib = this;
        ib.ccde = '';
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.SoldItems = [];
            UserService.getSoldItemsById($localStorage.loginData.user_ID).success(function(response, status) {
                if (status == 200) {
                    ib.SoldItems = response.data;
                }
                $ionicLoading.hide();
            });
        });

        ib.enterCode = function(code) {
            if (code.order_track_code == '' || code.order_track_code == null) {
                var trackcodePopup = $ionicPopup.show({
                    template: '<input type = "text" ng-model = "sold.track_code">',
                    title: 'Enter Tracking code',
                    scope: $scope,
                    buttons: [
                        {text: 'Cancel'},
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function(e) {
                                if (!ib.track_code) {
                                    e.preventDefault();
                                } else {
                                    return ib.track_code;
                                }
                            }
                        }
                    ]
                });
                trackcodePopup.then(function(res) {
                    if (res)
                    {
                        var data = {order_item_id: code.p_ID, order_seller_id: $localStorage.loginData.user_ID, order_track_code: res};
                        UserService.SetTrackCode(data).success(function(response, status) {
                            if (status == 200) {
                                ib.track_code = '';
                                UserService.getSoldItemsById($localStorage.loginData.user_ID).success(function(response, status) {
                                    if (status == 200) {
                                        ib.SoldItems = response.data;
                                    }
                                });
                                ionicToast.show(response.msg, 'top', false, 2500);
                            }
                        });
                    }
                });
            } else
            {

                var alertPopup = $ionicPopup.alert({
                    title: 'Tracking Code',
                    template: '<h3 class="subs-sub-title coupon-code-btn">You have already set Tracking code "' + code.order_track_code + '" for this product.</h3>'
                });
                alertPopup.then(function(res) {
                });
            }
        };
    }
})();
