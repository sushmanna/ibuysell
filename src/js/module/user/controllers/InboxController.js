(function() {
    angular.module('ibuysell').controller('InboxController', InboxController);
    InboxController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage', '$stateParams', '$q'];
    function InboxController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage, $stateParams, $q) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.message = {};
            var data = {user_id: $localStorage.loginData.user_ID};
            UserService.getInbox(data).success(function(response, status)
            {
                if (status == 200) {
                    ib.message = response.data;
                }
                $ionicLoading.hide();
            });
        });
    }
})();
