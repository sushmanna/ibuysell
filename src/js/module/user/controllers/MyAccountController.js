(function() {
    angular.module('ibuysell').controller('MyAccountController', MyAccountController);
    MyAccountController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', '$location', '$ionicLoading', '$cordovaImagePicker', '$ionicPopup', 'ionicToast','UserService', 'serverUrls', 'FirebaseService'];
    function MyAccountController($scope, $rootScope, $ionicModal, $timeout, $localStorage, $location, $ionicLoading, $cordovaImagePicker, $ionicPopup, ionicToast,UserService, serverUrls, FirebaseService) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.popdata = {};
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            UserService.accountpopdata().success(function(response, status)
            {          
                if (status== 200) {
                    ib.popdata = response.data;
                }
                $ionicLoading.hide();
            });
        });


        $scope.$on('$ionicView.enter', function() {
            // Code you want executed every time view is opened
            ib.logindata = $localStorage.loginData;
            ib.logindata.user_img = ib.logindata.user_img != null ? ib.logindata.user_img : 'img/user-no-img.png';
        });
        ib.uploadImage = function()
        {
            var options = {
                maximumImagesCount: 1,
                width: 200,
                height: 200,
                quality: 100
            };
            $cordovaImagePicker.getPictures(options)
            .then(function(results) {
                if( results.length > 0){
                    $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                    var data = {userId: $localStorage.loginData.user_ID, profile_image: results[0]};
                    UserService.setImage(data).then(function(response)
                    {
                        var res=JSON.parse(response.response);
                        $ionicLoading.hide();
                        if (response.responseCode== 200) {
                            ionicToast.show(res.msg, 'top', false, 3000);
                            $localStorage.loginData=res.data;
                            ib.logindata = $localStorage.loginData;
                            ib.logindata.user_img = ib.logindata.user_img != null ? ib.logindata.user_img : 'img/user-no-img.png';
                        }
                    });
                }

            }, function(error) {
                ionicToast.show('Upload image failed.Please Select image again', 'top', false, 3000);
            })
            .then(function() {
                
            });
        };

        ib.logout = function()
        {
            delete $localStorage.loginData;
            delete $rootScope.loginData;
            delete $localStorage.lastState;
            delete $localStorage.lastStateUrl;
            $location.path("/app/login");
        };

        ib.openLink = function( frm){
            if( frm == 'bulk-upload'){
                //window.open(serverUrls.bulkupload, '_blank', 'location=yes');
                var alertPopup = $ionicPopup.alert({
                    title: ib.popdata.contain.bulk_upload.title,
                    template: ib.popdata.contain.bulk_upload.html_data
                });
                alertPopup.then(function(res) {});
            } else if( frm == 'print-label'){
                var alertPopup = $ionicPopup.alert({
                    title: ib.popdata.contain.print_label.title,
                    template: ib.popdata.contain.print_label.html_data
                });
                alertPopup.then(function(res) {});
            }
            return false;
        };
    }
})();
