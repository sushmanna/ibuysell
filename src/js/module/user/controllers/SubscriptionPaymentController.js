(function() {
    angular.module('ibuysell').controller('SubscriptionPaymentController', SubscriptionPaymentController);
    SubscriptionPaymentController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', 'UserService', 'DataService', 'ProductService', '$ionicLoading', 'ionicToast', '$location', '$cordovaInAppBrowser', '$q', 'serverUrls'];
    function SubscriptionPaymentController($scope, $rootScope, $ionicModal, $timeout, $localStorage, UserService, DataService, ProductService, $ionicLoading, ionicToast, $location, $cordovaInAppBrowser, $q, serverUrls) {
        var ib = this;

        ib.paymentForm = {};

        $scope.$on('$ionicView.beforeEnter', function() {
        	$ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            
            ib.paymentForm.cart_temp_token = DataService.getData().ch_token;
            ib.paymentForm.subscription_id = DataService.getData().subscription_id;
            ib.paymentForm.subscription_term = DataService.getData().subscription_term;

        	UserService.getSubscriptionPlanById( {subscription_ID: ib.paymentForm.subscription_id}).success(function(response, status) {
                $ionicLoading.hide();
                if (status == 200) {
                    ib.paymentForm.plan_details = response.data;
                    angular.forEach(ib.paymentForm.plan_details.plans, function(value, key) {
						if( value.term == ib.paymentForm.subscription_term){
							ib.paymentForm.per_month_price = value.per_month_price;
							ib.paymentForm.display_name = value.display_name;
							ib.paymentForm.total_price = value.total_price;
							ib.paymentForm.paypal_plan_id = value.paypal_plan_id;
						}
					});
                }
                console.log( ib.paymentForm);
            });
    	});

        ib.payNow = function() {
            $ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            var options = {
                location: 'no',
                clearcache: 'yes',
                toolbar: 'no'
            };
            var callexit = false;
            $cordovaInAppBrowser.open(serverUrls.checkout + '/paymentsubscription?user_id=' + $localStorage.loginData.user_ID + '&cart_temp_token=' + ib.paymentForm.cart_temp_token, '_blank', options).then(function(event) {
                console.log('Success');
            }).catch(function(event) {
                console.log('Error');
            });
            $scope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
                if (event.url.match("checkout/close")) {
                    $cordovaInAppBrowser.close();
                }
            });
            $scope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                // Call the external API
                if (!callexit) {
                    callexit = true;
                    var data = {
                        user_id: $localStorage.loginData.user_ID,
                        cart_temp_token: ib.paymentForm.cart_temp_token
                    };
                    ProductService.orderPaymentCheck(data).success(function(response, status) {
                        $ionicLoading.hide();
                        ionicToast.show(response.msg, 'top', false, 2500);
                        $timeout(function() {
                            DataService.setData({});
                            $location.path("/app/categoryall");
                        }, 3000);
                    });
                }
            });
        };
    }
})();