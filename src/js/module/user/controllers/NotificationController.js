(function() {
    angular.module('ibuysell').controller('NotificationController', NotificationController);
    NotificationController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage'];
    function NotificationController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage) {
        var ib = this;
        ib.notifications={};
        $scope.$on('$ionicView.enter', function() {
            ib.notifications={};
            var data = {user_id: $localStorage.loginData.user_ID}
            UserService.getAllNotification(data).success(function(response, status)
            {
                if (status == 200) {
                    $rootScope.$broadcast('MessageCount', {MessageCount: 0});
                    ib.notifications = response.data;
                    //console.log(ib.notifications);
                }
            });
        });
        
    }
})();
