(function() {
    angular.module('ibuysell').controller('MyAddressController', MyAddressController);
    MyAddressController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$localStorage', '$q'];
    function MyAddressController($scope, $rootScope, $location, UserService, ionicToast, $ionicLoading, $localStorage, $q) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            var data = {user_id: $localStorage.loginData.user_ID};
            ib.userdata = {};
            ib.countrylist = {};

            var api = [];
            api[0] = UserService.getAllAddress(data);
            api[1] = UserService.countrylist();
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            $q.all(api).then(function(result, status){
                ib.userdata = result[0].data.data;
                ib.countrylist = result[1].data.data.country;
                $ionicLoading.hide();
            });
        });
        $scope.$on('$ionicView.enter', function() {
            ib.myaddress = {};
            ib.userdata.checkBoxValue = true;
            //console.log('enter');
        });

        ib.updateAddress = function() {
            if( ib.userdata.s_name === null || ib.userdata.s_name.length == 0){
                ionicToast.show('Missing shipping full name.', 'top', false, 2500);
            }
            else if( ib.userdata.s_address1 == null || ib.userdata.s_address1.length == 0){
                ionicToast.show('Missing shipping address 1.', 'top', false, 2500);
            }
            else if( ib.userdata.s_city == null || ib.userdata.s_city.length == 0){
                ionicToast.show('Missing shipping city.', 'top', false, 2500);
            }
            else if( ib.userdata.s_state == null || ib.userdata.s_state.length == 0){
                ionicToast.show('Missing shipping state.', 'top', false, 2500);
            }
            else if( ib.userdata.s_country === null || ib.userdata.s_country.length == 0){
                ionicToast.show('Missing shipping country.', 'top', false, 2500);
            }
            else if( ib.userdata.s_zip === null || ib.userdata.s_zip.length == 0){
                ionicToast.show('Missing shipping zip.', 'top', false, 2500);
            }
            else if( !ib.userdata.checkBoxValue && (ib.userdata.b_name === null || ib.userdata.b_name.length == 0)){
                ionicToast.show('Missing billing full name.', 'top', false, 2500);
            }
            else if( !ib.userdata.checkBoxValue && (ib.userdata.b_address1 === null || ib.userdata.b_address1.length == 0)){
                ionicToast.show('Missing billing address 1.', 'top', false, 2500);
            }
            else if( !ib.userdata.checkBoxValue && (ib.userdata.b_city === null || ib.userdata.b_city.length == 0)){
                ionicToast.show('Missing billing city.', 'top', false, 2500);
            }
            else if( !ib.userdata.checkBoxValue && (ib.userdata.b_state === null || ib.userdata.b_state.length == 0)){
                ionicToast.show('Missing billing state.', 'top', false, 2500);
            }
            else if( !ib.userdata.checkBoxValue && (ib.userdata.b_country === null || ib.userdata.b_country.length == 0)){
                ionicToast.show('Missing billing country.', 'top', false, 2500);
            }
            else if( !ib.userdata.checkBoxValue && (ib.userdata.b_zip === null || ib.userdata.b_zip.length == 0)){
                ionicToast.show('Missing billing zip.', 'top', false, 2500);
            }
            else{
                $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                if( ib.userdata.checkBoxValue){
                    ib.userdata.b_name = ib.userdata.s_name;
                    ib.userdata.b_address1 = ib.userdata.s_address1;
                    ib.userdata.b_address2 = ib.userdata.s_address2;
                    ib.userdata.b_city = ib.userdata.s_city;
                    ib.userdata.b_state = ib.userdata.s_state;
                    ib.userdata.b_country = ib.userdata.s_country;
                    ib.userdata.b_zip = ib.userdata.s_zip;
                    ib.userdata.b_phone = ib.userdata.s_phone;
                }
                ib.userdata.user_id = $localStorage.loginData.user_ID;
                UserService.updateAddress(ib.userdata).success(function(response, status)
                {
                    $ionicLoading.hide();
                    if (status == 200) {
                        ionicToast.show(response.msg, 'top', false, 2500);
                    } else
                    {
                        ionicToast.show(response.msg, 'top', false, 2500);
                    }
                });
            }
        }
    }
})();
