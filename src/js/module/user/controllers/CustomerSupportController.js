(function() {
    angular.module('ibuysell').controller('CustomerSupportController', CustomerSupportController);
    CustomerSupportController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', 'ionicToast', 'UserService', '$localStorage', '$ionicLoading'];
    function CustomerSupportController($scope, $rootScope, $ionicModal, $timeout, ionicToast, UserService, $localStorage, $ionicLoading) {
        var ib = this; 
        ib.frm = {};
        ib.submitMessage = function()
        {
            if( !angular.isDefined(ib.frm.issue) || ib.frm.issue.length == 0){
                ionicToast.show('Please select an issue.', 'top', false, 2500);
            } else if( !angular.isDefined(ib.frm.subject) || ib.frm.subject.length == 0) {
                ionicToast.show('Missing title.', 'top', false, 2500);
            } else if( !angular.isDefined(ib.frm.message) || ib.frm.message.length == 0) {
                ionicToast.show('Missing message.', 'top', false, 2500);
            } else {
                $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                var subject=ib.frm.issue+': '+ib.frm.subject;
                var data = {posted_to: 0, posted_by: $localStorage.loginData.user_ID, msg_content: ib.frm.message, msg_type: 'support',msg_subject: subject};
                UserService.sendMessageOrSupport(data).success(function(response, status)
                {
                    $ionicLoading.hide();
                    if (status == 200) {
                        ib.frm = {};
                        ionicToast.show(response.msg, 'top', false, 2500);
                    } 
                    else
                    {
                        ionicToast.show(response.msg, 'top', false, 2500);
                    }
                });
            }            
        }
    }
})();
