(function() {
    angular.module('ibuysell').controller('RegistrationController', RegistrationController);
    RegistrationController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$ionicLoading', 'UserService', 'ionicToast', '$location'];
    function RegistrationController($scope, $rootScope, $ionicModal, $timeout, $ionicLoading, UserService, ionicToast, $location) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function(){
            ib.logindata = {fname: '', username: '', emailid : '', password : ''};
        });
        //normal register
        ib.register = function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.logindata.user_img = null;
            UserService.register(ib.logindata).success(function(response, status)
            {
                $ionicLoading.hide();
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', false, 2500);
                    $location.path("/app/login");
                }
                else
                {
                    ionicToast.show(response.msg, 'top', false, 2500);
                }
            });
        };
    }
})();
