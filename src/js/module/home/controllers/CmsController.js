(function() {
    angular.module('ibuysell').controller('CmsController', CmsController);
    CmsController.$inject = ['$scope', '$rootScope', 'UserService', '$stateParams','$ionicLoading'];
    function CmsController($scope, $rootScope, UserService, $stateParams,$ionicLoading) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            var cmsKey= $stateParams.cmsKey;
                UserService.getCms(cmsKey).success(function(response,status) {
                ib.cmsData = response.data;
                $ionicLoading.hide();
            });
        });
       
    }
})();