(function() {
    angular.module('ibuysell').controller('HomeController', HomeController);
    HomeController.$inject = ['$scope', '$rootScope','$ionicSlideBoxDelegate'];
    function HomeController($scope, $rootScope,$ionicSlideBoxDelegate) {
        var ib=this;
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.slides=['img/banner02.jpg','img/banner01.jpg','img/banner03.jpg','img/banner04.jpg'];
        });
        $scope.$on('$ionicView.afterEnter', function() {
            ib.changeslide();
        });
        ib.changeslide=function()
        {
           $ionicSlideBoxDelegate.next();
        };        
    }
})();