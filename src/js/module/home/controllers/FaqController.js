(function() {
    angular.module('ibuysell').controller('FaqController', FaqController);
    FaqController.$inject = ['$scope', '$rootScope', 'UserService', '$q', '$ionicModal','$ionicLoading','$ionicScrollDelegate'];
    function FaqController($scope, $rootScope, UserService, $q, $ionicModal,$ionicLoading, $ionicScrollDelegate) {
        var ib = this;
        $ionicModal.fromTemplateUrl('js/module/home/templates/faqModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.faqModal = modal;
        });
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.faqList={};
            ib.detailsfaq={};
            ib.listdetailsfaq={};
            var api = [];
            api[0] = UserService.getMainFaqList();
            api[1] = UserService.getFaqList();
            $q.all(api).then(function(result) {
                ib.faqList=result[0].data.data;
                ib.detailsfaq=result[1].data.data;
                $ionicLoading.hide();
            });
        });

        ib.closeFaqModal = function() {
            ib.index='';
            ib.listdetailsfaq={};
            ib.faqModal.hide();
        };
        ib.openFaqModal = function(index) {
            ib.index=index;
            ib.listdetailsfaq=ib.detailsfaq[index];
            ib.faqModal.show();
            $ionicScrollDelegate.$getByHandle('faqModalContent').scrollTop(true);
        };


    }
})();