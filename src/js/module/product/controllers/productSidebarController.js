(function() {
    "use strict";
    angular.module('ibuysell').controller('productSidebarController', productSidebarController);
    productSidebarController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', '$location', 'ProductService', 'UserService'];
    function productSidebarController($scope, $rootScope, $ionicModal, $timeout, $localStorage, $location, ProductService, UserService) {
        var ib = this;
        ib.MessageCount = 0;
        ib.CartCount = 0;
        $scope.$on('$ionicView.enter', function() {
            $rootScope.loginData=$localStorage.loginData;
            ib.currentCat = '';
            ProductService.getCategories(ib.logindata).success(function(response, status)
            {
                if (status == 200) {
                    $rootScope.categories = response.data.allCategories;
                    $rootScope.mainCategories = response.data.mainCategories;
                    ib.categories = response.data.allCategories;
                    ib.mainCategories = response.data.mainCategories;
                }
            });
            if ($localStorage.loginData) {
                UserService.getCartCountByUser($localStorage.loginData.user_ID).success(function(response, status)
                {
                    if (status == 200) {
                        $rootScope.loginData.CartCount = response.data;
                        ib.CartCount = response.data;
                    }
                });
            }
            $scope.$on('MessageCount', function(event, args) {
                ib.MessageCount = args.MessageCount;
            });
        });


        ib.toggleCategory = function(key)
        {
            if (ib.currentCat == key)
            {
                ib.currentCat = '';
            } else
            {
                ib.currentCat = key;
            }
        };
        ib.isCategoryShown = function(category)
        {
            if (ib.currentCat == category)
            {
                return true;
            }
            else {
                return false;
            }
        };
    }
})();
