(function() {
    angular.module('ibuysell').controller('productListController', productListController);
    productListController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$stateParams', '$ionicLoading', 'FirebaseService', '_'];
    function productListController($scope, $rootScope, $ionicModal, $timeout, $stateParams, $ionicLoading, FirebaseService, _) {
        var ib = this;
        ib.totalList = [];
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.list = [];
            ib.counter = 0;
            FirebaseService.getProductfromfirebase().then(function(data) {
                var l = _.filter(data, function(obj) {
                    if ((obj.parent_category_id == $stateParams.catId || obj.p_cat_category_id == $stateParams.catId) && obj.p_is_frozen == 'N' && obj.p_on_live_sale == 'N' && obj.p_is_sold == 'N') {
                        return true;
                    }
                });
                ib.totalList = l;
                ib.populateList();
            });
            FirebaseService.getCategoryByID($stateParams.catId).then(function(data) {
                $ionicLoading.hide();
                ib.category = data.cat_name;
                if (data.cat_parent_id) {
                    FirebaseService.getCategoryByID(data.cat_parent_id).then(function(parent_data) {
                        ib.parent_category = parent_data.cat_name;
                        ib.parent_category_id = parent_data.cat_ID;
                    });
                }
            });

        });
        ib.populateList = function() {
            for (var i = 0; i <= 3; i++) {
                if (ib.totalList[ib.counter] != null)
                {
                    if(ib.totalList[ib.counter].p_ID){
                        ib.list.push(ib.totalList[ib.counter]);
                        ib.counter++;
                    }
                }
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
        }
        ib.canWeLoadMoreContent = function() {
            return (ib.counter > ib.totalList.length) ? false : true;
        }
        ib.populateList();
    }
})();
