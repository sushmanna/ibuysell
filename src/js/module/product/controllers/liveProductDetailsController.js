(function() {
    angular.module('ibuysell').controller('liveProductDetailsController', liveProductDetailsController);
    liveProductDetailsController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth','$ionicLoading','FirebaseService','$stateParams','ProductService','$q'];
    function liveProductDetailsController($scope, $rootScope, $ionicModal, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth,$ionicLoading,FirebaseService, $stateParams, ProductService,$q) {
        var ib = this;
        ib.openclass = 'cssSlideClose';
//        var ref = new Firebase("https://ibuysell.firebaseio.com/");
//        var ref1 = new Firebase("https://ibuysell.firebaseio.com/product");
        
        ib.data = [
            {title: 'SHORT DESCRIPTION', description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'},
            {title: 'SELLER INFO', description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'},
            {title: 'DELEVERY OPTION', description: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<p>'}
        ];
        
        $scope.$on('$ionicView.beforeEnter', function(){
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.productdetails={};
            ib.productId = $stateParams.productId;
            var api = [];
            api[0] = FirebaseService.getProductById(ib.productId);
            api[1] = ProductService.getPresetCount(ib.productId);
            $q.all(api)
                    .then(function(result){
                        ib.productFirebaseKey = result[0].key;
                        ib.productdetails = result[0].value;
                        ib.productdetails.tagcount = result[1].data.data;
                        ib.mainThumbImage = ib.productdetails.img_url[0];
                        FirebaseService.manageProductOnlineViewer(result[0].key, parseInt(result[0].value.online_viewer + 1));
                        ib.data = [
                            {title: 'SHORT DESCRIPTION', description: result[0].value.p_description},
                            {title: 'SELLER INFO', description: result[0].value.p_description},
                            {title: 'DELEVERY OPTION', description: result[0].value.p_shipping_method}
                        ];
                    })
                    .then(function(){
                        FirebaseService.getRelatedProduct(ib.productdetails.p_cat_category_id).then(function(response){
                            $ionicLoading.hide();
                            var data = _.sortBy(response, 'p_created').reverse();
                            data = data.slice(0, 6);
                            ib.relatedProduct = data;
                        });
                    });
        });
        ib.setThumb = function(thumb){
            ib.mainThumbImage = thumb;
        };
        
//        ib.fblogin = function()
//        {
//            // create an instance of the authentication service
//            var auth = $firebaseAuth(ref);
//            // login with Facebook
//            auth.$authWithOAuthPopup("facebook").then(function(authData) {
//                console.log("Logged in as:", authData.uid);
//                console.log("Logged in as:", authData);
//            }).catch(function(error) {
//                console.log("Authentication failed:", error);
//            });
//        }

//        ib.add = function()
//        {
//            console.log('add');
//            ib.data = $firebaseObject(ref);
//            console.log(ib.data);
//            var syncObject = $firebaseArray(ref1);
//
//            var ss = {
//                name: 'Product' + Math.round(Math.random() * 1000000),
//                price: Math.round(Math.random() * 1000)
//            };
//            syncObject.$add(ss).then(function(ref) {
//                var id = ref.key();
//                console.log("added record with id " + id);
//                console.log(syncObject.$indexFor(id)); // returns location in the array
//            });
//            ib.messages = $firebaseArray(ref1);
//        }
//        ib.Update = function()
//        {
//
////            var list = $firebaseArray(ref1);
////            var someItem = list.$getRecord('-K2LsnmMS9fvaT6ckrCi');
//
//            var rec = ib.messages.$getRecord('-K2LsnwrxrsxOpZOmvWc');
//            rec.name = 'newField';
//            console.log(rec);
//            ib.messages.$save(rec).then(function(ref) {
//                console.log('$scope.items is now', $scope.items);
//            });
//            //ib.messages.$save(id);
////            rec.name = 'newField';
////            list.$save(someItem).then(function(ref) {
////                console.log('$scope.items is now', $scope.items);
////            });
//
//
////      [{"name":"Product251698","price":187,"$id":"-K2LsnmMS9fvaT6ckrCi","$priority":null},{"name":"Product880737","price":893,"$id":"-K2LsnpbNlYTJyK_-_O7","$priority":null},{"name":"Product292395","price":809,"$id":"-K2Lsnsik-oVVsxRAIJ9","$priority":null},{"name":"Product194230","price":995,"$id":"-K2LsnwrxrsxOpZOmvWc","$priority":null},{"name":"Product619091","price":179,"$id":"-K2Lsnzu2xWg566YYC8b","$priority":null},{"name":"Product239464","price":566,"$id":"-K2Lso1jBiiN_CndCTxO","$priority":null},{"name":"Product402420","price":726,"$id":"-K2LwNW95Ad7S18fllIL","$priority":null}]
//
//
//
////            console.log('Update');
////            var someItem = itemsList.$getRecord('-K2L_BmnO1CYKipuYLrX');
////            var newField = prompt('Enter new value for newField');
////            someItem.newField = newField;
////            itemsList.$save(someItem).then(function(ref) {
////                console.log('$scope.items is now', $scope.items);
////            });
////            ;
//        }
//        ib.Delete1 = function(id)
//        {
//            ib.messages.$remove(id);
//        }
//        ib.Delete = function()
//        {
////            ib.data = $firebaseObject(ref);
////            console.log(ib.data);
//            var syncObject = $firebaseArray(ref1);
//            syncObject.$remove(2).then(function(ref) {
//                console.log(ref);
//                console.log('ref');
//            });
////            var ss = {
////                name: 'Product' + Math.round(Math.random() * 1000000),
////                price: Math.round(Math.random() * 1000)
////            };
////            syncObject.$add(ss).then(function(ref) {
////                var id = ref.key();
////                console.log("added record with id " + id);
////                console.log(syncObject.$indexFor(id)); // returns location in the array
////            });
////            ib.messages = $firebaseArray(ref1);
////            list.$remove(item).then(function(ref) {
////                if( ref1.key() === item.$id) console.log('Yahoo'); // true
////            })
//        }
//        ib.Read = function()
//        {
//            //            console.log('Read');
//            //            $scope.data = $firebaseObject(ref);
//            //            console.log($scope.data);
//            var syncObject = $firebaseArray(ref1);
//            ib.messages = $firebaseArray(ref1);
//
//
//        }
    }
})();