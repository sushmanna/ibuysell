(function() {
    angular.module('ibuysell').controller('livesaleController', livesaleController);
    livesaleController.$inject = ['$scope', '$rootScope', '$ionicModal', '$ionicPopup', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth','FirebaseService','$stateParams','$interval', '$localStorage', 'ProductService', 'ionicToast', '$state','$ionicLoading','$ionicScrollDelegate','$ionicPlatform', '$ionicHistory', '_'];
    function livesaleController($scope, $rootScope, $ionicModal, $ionicPopup, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, FirebaseService,$stateParams,$interval, $localStorage, ProductService, ionicToast, $state,$ionicLoading, $ionicScrollDelegate,$ionicPlatform, $ionicHistory, _) {
        var ib = this;
        ib.liveProducts = {};
        ib.timeRemaining = 0;
        ib.upcoming = {};
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.list = [];
            ib.counter = 0;
            /*ib.callcat = 0;
            ib.livecategorycount = -1;*/
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            FirebaseService.getLiveSaleDataByProduct().then(function(data) {
              //console.log('fetched');
              ib.liveProducts = data;
              angular.forEach(ib.liveProducts,function(value, key){
                  ib.startLiveSale(key);
              });
              $ionicLoading.hide();
            });
        });
        $scope.$on('updateLiveSaleProductList', function(event, args){
            var resp = args;
            //console.log( args);
            /*if( !ib.callcat){
              ib.callcat = 1;
              FirebaseService.getLiveSaleCategories().then(function(response) {
                ib.livecategorycount = 0;
                angular.forEach( response, function(key, value){
                  if( value) ib.livecategorycount = ib.livecategorycount + 1;
                });
              });
            }*/

            //if( _.size(resp) == ib.livecategorycount){
              //ib.livecategorycount = -1;
              //ib.callcat = 0;
              $timeout(function() {
                ib.liveProducts = args;
                $ionicScrollDelegate.scrollTop();
                angular.forEach(ib.liveProducts,function(value, key){
                  ib.startLiveSale(key);
                });
              }, 3000);
            //}
        });
        $scope.$on('$ionicView.leave', function() {
            $interval.cancel(ib.priceInterval);
            ib.priceInterval = null;
        });
        
//        ib.filterByLiveSale = function(){
//          
//            //var l = _.where(args, {p_live_start_time: 'Y'});
//            ib.currentTime = (Math.floor(Date.now() / 1000));
//            var l = _.filter(ib.allProducts, function(obj){
//                // return true for every valid entry!
//                //console.log(obj.p_live_start_time+'    now:'+Math.floor(Date.now() / 1000));
//                if(parseInt(obj.p_live_start_time) <= ib.currentTime &&  (parseInt(obj.p_live_start_time)+liveSaleTime) >= ib.currentTime) {
//                  //ib.timeRemaining = obj.p_live_start_time - ib.currentTime + liveSaleTime;
//                  //console.log(obj);
//                  //ib.startLiveSale();//console.log(obj);
//                  return true;
//                }
//            }).sort(function(a,b){
//                if (a.p_live_start_time > b.p_live_start_time) return 1;
//                if (b.p_live_start_time > a.p_live_start_time) return -1;
//                return 0;
//            });
//            ib.totalList = l;
//            //add the category special name to the products
//            angular.forEach(ib.totalList,function(value, key){
//              //console.log(value.p_cat_category_id);
//              FirebaseService.getCategoryByID(parseInt(value.p_cat_category_id)).then(function(data) {
//                ib.livedata[key].current.cat_special_words = data.cat_special_words;
//              });
//              ib.startLiveSale(key);
//            });
//            //console.log(ib.totalList);
//        };
        
        ib.startLiveSale = function(key){
          //console.log(ib.liveProducts[key].parent_category_id);
          FirebaseService.getUpcomming(ib.liveProducts[key].parent_category_id,2).then(function(data) {
            ib.upcoming[key] = data;
            //console.log(ib.upcoming[key]);
            /*console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            console.log( 'Category Id : Product Id -> '+data[0].cat_ID+' : '+data[0].p_ID);
            console.log( 'Category Id : Product Id -> '+data[1].cat_ID+' : '+data[1].p_ID);
            console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@');*/
          });
          //console.log('jjjjjjjjjjjjjjjjj');
          ib.currentTime = (Math.floor(Date.now() / 1000));
          ib.liveProducts[key].timeRemaining = ib.liveProducts[key].p_live_start_time - ib.currentTime + liveSaleTime;
          getPriceDecrement(key);
          if(!ib.priceInterval)
            ib.priceInterval = $interval(updatePrice,1000);
        };
        var getPriceDecrement = function(key) {
            var maxDrop = (ib.liveProducts[key].p_selling_price - ib.liveProducts[key].p_min_selling_price);
            ib.liveProducts[key].eachDrop = (maxDrop / liveSaleTime);
            ib.liveProducts[key].currentPrice = parseFloat((parseFloat(ib.liveProducts[key].p_selling_price) - ((liveSaleTime - ib.liveProducts[key].timeRemaining) * ib.liveProducts[key].eachDrop)).toFixed(2));
        };
        
        var updatePrice = function(){
          angular.forEach(ib.liveProducts,function(value, key){
            if(ib.liveProducts[key].timeRemaining>0){
              ib.liveProducts[key].timeRemaining--;
              ib.liveProducts[key].currentPrice = (ib.liveProducts[key].currentPrice-ib.liveProducts[key].eachDrop).toFixed(2);
              ib.liveProducts[key].currentSaving = parseFloat(ib.liveProducts[key].p_max_selling_price) - ib.liveProducts[key].currentPrice;
              ib.liveProducts[key].currentPercentOff = parseInt(ib.liveProducts[key].currentSaving/parseFloat(ib.liveProducts[key].p_max_selling_price)*100);
            } else {
              //close live sale confirm popup
              if (ib.confirmPopup) {
                  ib.confirmPopup.close();
              }
              ib.liveProducts[key].ended = true;
            }
          });
        };
        // A confirm dialog
        ib.confirmFreeze = function(key) {
            ib.liveProducts[key].freezingItem = true;
            ib.liveProducts[key].freezePrice = angular.copy(ib.liveProducts[key].currentPrice);
            ib.liveProducts[key].freezePercentOff = angular.copy(ib.liveProducts[key].currentPercentOff);
            ib.stopLiveSale(key, true);//send true to not update status in firebase
            ib.confirmPopup = $ionicPopup.confirm({
                title: 'Freezing item at: $' + ib.liveProducts[key].freezePrice,
                template: 'Are you sure you want to Freeze this Item?'
            });
            ib.confirmPopup.then(function(res) {
                if (res) {
                    // Don't freeze item if its already frozen!
                    FirebaseService.getProductById(ib.liveProducts[key].p_ID).then(function(response) {
                      //console.log(response);
                      //console.log(response[ib.liveProducts[key].p_ID].value);
                      if(response[ib.liveProducts[key].p_ID].value.p_is_frozen=='N'){
                        ib.freezeItem(key);
                        ib.stopLiveSale(key);
                      } else {
                        ionicToast.show('Sorry! '+response[ib.liveProducts[key].p_ID].value.p_title+' is already Sold!', 'top', false, 2500);
                      }
                    });
                    //Freeze Item for all
                    
                } else {
                    ib.liveProducts[key].freezingItem = false;
                }
            });
        };
        ib.stopLiveSale = function(key,noFirebaseUpdate) {
            //console.log('ib.liveSaleActive:'+ib.liveSaleActive)
            if (ib.confirmPopup) {
                ib.confirmPopup.close();
                //console.log('close called');
            }
            
            if (!noFirebaseUpdate) {
                FirebaseService.updateProduct(key, {p_on_live_sale: 'N'});
            }
        };
        ib.freezeItem = function(key) {
            ib.currentTime = (Math.floor(Date.now() / 1000));
            ib.freezePrice = ib.liveProducts[key].freezePrice;
            ib.product_id = ib.liveProducts[key].p_ID;
            ib.p_title = ib.liveProducts[key].p_title;
            var data = {p_ID: ib.liveProducts[key].p_ID, p_is_frozen: 'Y', p_frozen_by: parseInt($localStorage.loginData.user_ID), p_frozen_price: ib.freezePrice, p_frozen_time: ib.currentTime, p_on_live_sale:'N'};
            
            FirebaseService.updateProduct(key, data);
            //FirebaseService.updateLivesale(key, data);
            //console.log(ib.freezePrice);
            ProductService.updateProduct(data).success(function(response, status)
            {
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', false, 2500);
                    // Add freezed item to cart
                    var data = {buyer_id: $rootScope.loginData.user_ID, product_id: ib.product_id, price: ib.freezePrice, product_title: ib.p_title, is_frozen:1};
                    ProductService.addToCart(data).success(function(response, status){
                      if (status == 200) {
                        ionicToast.show(response.msg, 'top', false, 2500);
                        $state.go('app.cart');
                      } else 
                      {
                        ionicToast.show(response.msg, 'top', false, 2500);
                      }
                    });
                }
            });
            
            
        };
        ib.priceInterval = $interval(updatePrice,1000);
    }
})();