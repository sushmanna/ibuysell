(function() {
    angular.module('ibuysell').controller('paymentController', paymentController);
    paymentController.$inject = ['$scope', '$rootScope', '$ionicModal', '$ionicLoading', '$cordovaInAppBrowser', '$location', 'ionicToast', '$timeout', '$localStorage', 'ProductService', 'DataService', 'serverUrls'];

    function paymentController($scope, $rootScope, $ionicModal, $ionicLoading, $cordovaInAppBrowser, $location, ionicToast, $timeout, $localStorage, ProductService, DataService, serverUrls) {
        var ib = this;
        $scope.$on('$ionicView.beforeEnter', function() {
            var crttemp = DataService.getData();
            ib.cartTempToken = crttemp.cart_temp_token;
            ib.totalCartAmount = crttemp.total_cart_amount;
            ib.isPayment = 'card';
            ib.paymentform = {
                card_holder_name: '',
                card_no: '',
                card_type: '',
                cvv: '',
                month: '',
                year: ''
            };
            ib.years = [];
            ib.months = [{
                id: 1,
                label: 'January'
            }, {
                id: 2,
                label: 'February'
            }, {
                id: 3,
                label: 'Merch'
            }, {
                id: 4,
                label: 'April'
            }, {
                id: 5,
                label: 'May'
            }, {
                id: 6,
                label: 'June'
            }, {
                id: 7,
                label: 'July'
            }, {
                id: 8,
                label: 'August'
            }, {
                id: 9,
                label: 'September'
            }, {
                id: 10,
                label: 'October'
            }, {
                id: 11,
                label: 'November'
            }, {
                id: 12,
                label: 'December'
            }];
            ib.cardtypes = [{
                id: 'visa',
                label: 'VISA'
            }, {
                id: 'master',
                label: 'MASTER'
            }];
            var currentYear = new Date().getFullYear();
            for (var i = currentYear; i < currentYear + 31; i++) {
                ib.years.push({
                    id: i,
                    label: i
                });
            }
        });
        ib.togglePayment = function(str) {
            ib.isPayment = str;
        };
        ib.payNow = function() {
            if (ib.isPayment == 'paypal') {
                ib.openPaymentWindow();
            } else if (ib.isPayment == 'card') {
                if (ib.paymentform.card_holder_name.length == 0) {
                    ionicToast.show('Missing card holder name.', 'top', false, 2500);
                } else if (ib.paymentform.card_type.length == 0) {
                    ionicToast.show('Missing card type.', 'top', false, 2500);
                } else if (ib.paymentform.card_no == null) {
                    ionicToast.show('Missing card number.', 'top', false, 2500);
                } else if (String(ib.paymentform.card_no).length != 16) {
                    ionicToast.show('Invalid card number.', 'top', false, 2500);
                } else if (ib.paymentform.cvv == null) {
                    ionicToast.show('Missing cvv number.', 'top', false, 2500);
                } else if (String(ib.paymentform.cvv).length > 4) {
                    ionicToast.show('Invalid cvv number.', 'top', false, 2500);
                } else if (ib.paymentform.month.length == 0) {
                    ionicToast.show('Missing expiry month.', 'top', false, 2500);
                } else if (ib.paymentform.year.length == 0) {
                    ionicToast.show('Missing expiry year.', 'top', false, 2500);
                } else {
                    $ionicLoading.show({
                        template: '<img src="img/loader.GIF">'
                    });
                    var data = {
                        user_id: $localStorage.loginData.user_ID,
                        ch_token: ib.cartTempToken,
                        ch_payment_type: 'card',
                        ch_card_details: JSON.stringify(ib.paymentform)
                    };
                    ProductService.saveCartTempPaymentSave(data).success(function(response, status) {
                        $ionicLoading.hide();
                        if (status == 200) {
                            ib.openPaymentWindow();
                        }
                    });
                }
            }
        };
        ib.openPaymentWindow = function() {
            $ionicLoading.show({
                template: '<img src="img/loader.GIF">'
            });
            var options = {
                location: 'no',
                clearcache: 'yes',
                toolbar: 'no'
            };
            var callexit = false;
            $cordovaInAppBrowser.open(serverUrls.checkout + '?user_id=' + $localStorage.loginData.user_ID + '&cart_temp_token=' + ib.cartTempToken, '_blank', options).then(function(event) {
                console.log('Success');
            }).catch(function(event) {
                console.log('Error');
            });
            $scope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
                if (event.url.match("checkout/close")) {
                    $cordovaInAppBrowser.close();
                }
            });
            $scope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                // Call the external API
                if (!callexit) {
                    callexit = true;
                    var data = {
                        user_id: $localStorage.loginData.user_ID,
                        cart_temp_token: ib.cartTempToken
                    };
                    ProductService.orderPaymentCheck(data).success(function(response, status) {
                        $ionicLoading.hide();
                        ionicToast.show(response.msg, 'top', false, 2500);
                        $timeout(function() {
                            DataService.setData({});
                            $location.path("/app/myorder");
                        }, 3000);
                    });
                }
            });
        };
    }
})();