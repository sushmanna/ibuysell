(function() {
    angular.module('ibuysell').controller('productDetailsController', productDetailsController);
    productDetailsController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$stateParams', 'FirebaseService', 'ProductService', '$q', 'ionicToast', '$ionicLoading', '$ionicPopup', '$state', '$interval', 'UserService', '$localStorage'];
    function productDetailsController($scope, $rootScope, $ionicModal, $timeout, $stateParams, FirebaseService, ProductService, $q, ionicToast, $ionicLoading, $ionicPopup, $state, $interval, UserService, $localStorage) {
        var ib = this;
        ib.message = '';
        $ionicModal.fromTemplateUrl('js/module/product/templates/presetModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.presetModal = modal;
        });
        ib.mainThumbImage = '';
        ib.currentTime = Math.floor(Date.now() / 1000);
        //console.log(ib.currentTime);
        //ib.currentTime = 1449334820;
        ib.timeRemaining = 0;
        ib.onLiveSale = false;
        ib.openclass = 'cssSlideClose';
        ib.productdetails = {};
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.productdetails = {};
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.productId = parseInt($stateParams.firebaseId);
            ib.currentUserId = $localStorage.loginData.user_ID;
            var api = [];
            api[0] = FirebaseService.getProductById(ib.productId);
            api[1] = ProductService.getPresetCount(ib.productId, $localStorage.loginData.user_ID);
            $q.all(api)
                    .then(function(result) {
                        //console.log(result[0]);
                        ib.productFirebaseKey = result[0][ib.productId].key;
                        ib.productdetails = result[0][ib.productId].value;
                        angular.forEach( productdata, function( value, key){
                          if( key != ib.productId)
                            delete productdata[key];
                        });
                        //console.log(productdata);
                        
                        ib.tagcount = result[1].data.data.totalTag;
                        ib.tagcountaccess = result[1].data.data.userHasTag;

                        if (ib.productdetails.img_url)
                        {
                            ib.mainThumbImage = ib.productdetails.img_url[0];
                        }
                        else {
                            ib.mainThumbImage = 'img/product-no-img.png';
                        }
                        ib.img_url = ib.productdetails.img_url;

                    })
                    .then(function() {
                        ib.isOnLiveSale();
                        var api1 = [];
                        //api1[0] = FirebaseService.getRelatedProduct(ib.productdetails.p_cat_category_id, ib.productdetails.p_ID); 
                        api1[0] = ProductService.getRelatedproductById(ib.productdetails.p_ID, $localStorage.loginData.user_ID);
                        api1[1] = UserService.getSellerById(ib.productdetails.p_uid);
                        $q.all(api1).then(function(response) {
                            $ionicLoading.hide();
                            //var data = _.sortBy(response[0], 'p_created').reverse();
                            //data = data.slice(0, 6);
                            ib.relatedProduct = response[0].data.data;
                            var rate = response[1].data.data.seller_rating;
                            var rateString = '';
                            if (rate > 0)
                            {
                                for (var i = 0; i < rate; i++)
                                {
                                    rateString += '<i class="ion-ios-star"></i>';
                                }
                            }

                            var description = (response[1].data.data.bio != '' && response[1].data.data.bio != null) ? '<p><b>Description: </b>' + response[1].data.data.bio + '</p>' : '';
                            var website = (response[1].data.data.website != '' && response[1].data.data.website != null) ? '<p><b>Website: </b>' + response[1].data.data.website + '</p>' : '';
                            var rating = (rateString != '') ? '<p><b>Rating: </b>' + rateString + '</p>' : '';
                            ib.data = [
                                {title: 'Short Description', description: ib.productdetails.p_description},
                                {title: 'Seller Info', description: '<p><b>Username: </b> <a href="#app/mycloset/' + ib.productdetails.p_uid + '">' + response[1].data.data.username + '</a></p>' + description + website + rating
                                },
                                {title: 'Delivery Option', description: (ib.productdetails.p_shipping_method.toLowerCase() == 'stamps_com' ? 'USPS' : ib.productdetails.p_shipping_method)}
                            ];
                            FirebaseService.manageProductOnlineViewer(ib.productFirebaseKey, parseInt(ib.productdetails.online_viewer + 1));
                        });
                    });
        });
        $scope.$on('updatedProduct', function(event, args) {
            ib.productdetails = args[ib.productId].value;
//            FirebaseService.getLiveSaleData().then(function(data) {
//              ib.upcoming = data.live[ib.productdetails.parent_category_id]['upcoming'];
//            });
            ib.isOnLiveSale();
        });
        $scope.$on('$ionicView.leave', function() {
            if (ib.productdetails.online_viewer <= 0)
            {
                var count = 0;
            }
            else
            {
                var count = parseInt(ib.productdetails.online_viewer - 1);
            }
            ib.stopLiveSale(true);
            //console.log('leave');
            $interval.cancel(ib.priceDropInterval);
            $interval.cancel(ib.checkLiveSale);
            FirebaseService.manageProductOnlineViewer(ib.productFirebaseKey, count);
        });
        ib.closeTagModal = function() {
            ib.presetModal.hide();
        };
        ib.openTagModal = function() {
            if (ib.tagcountaccess)
            {
                ionicToast.show('You have already tag this product.', 'top', false, 2500);
            }
            else
            {
                ib.preset_price = 0;
                ib.presetModal.show();
            }
        };
        ib.submitPresetValue = function() {
            if (ib.tagcountaccess)
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Remove reminder',
                    template: 'Are you sure you remove this reminder?'
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                        var data = {user_id: $localStorage.loginData.user_ID, product_id: ib.productId};
                        ProductService.removePresetValue(data).success(function(response, status)
                        {
                            if (status == 200) {
                                $ionicLoading.hide();
                                ionicToast.show(response.msg, 'top', false, 2500);
                                ib.tagcountaccess = false;
                            }
                        });
                    }
                });
            }
            else
            {
                $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                var data = {p_ID: ib.productId, user_id: $localStorage.loginData.user_ID, preset_value: 0};
                ProductService.setPresetValue(data).success(function(response, status)
                {
                    if (status == 200) {
                        $ionicLoading.hide();
                        ionicToast.show(response.msg, 'top', false, 2500);
                        ib.tagcountaccess = true;
                    }
                });
            }
        };
        ib.freezeItem = function() {
            var data = {p_ID: ib.productId, p_is_frozen: 'Y', p_frozen_by: parseInt($localStorage.loginData.user_ID), p_frozen_price: parseFloat(ib.freezePrice), p_is_frozen:'Y', p_frozen_time: ib.currentTime, p_on_live_sale:'N'};
            FirebaseService.updateProduct(ib.productFirebaseKey, data);
            ProductService.updateProduct(data).success(function(response, status)
            {
                if (status == 200) {
                    //ib.presetModal.hide();
                    ionicToast.show(response.msg, 'top', false, 2500);
                    //ib.productdetails.tagcount = ib.productdetails.tagcount + 1;
                }
            });

            // Add freezed item to cart
            var data = {buyer_id: $rootScope.loginData.user_ID, product_id: ib.productdetails.p_ID, price: ib.freezePrice, product_title: ib.productdetails.p_title, is_frozen: 1};
            ProductService.addToCart(data).success(function(response, status) {
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', false, 2500);
                    $state.go('app.cart');
                } else
                {
                    ionicToast.show(response.msg, 'top', false, 2500);
                }
            });
        };
        ib.isOnLiveSale = function() {
            //console.log('SUSU');
            ib.currentTime = Math.floor(Date.now() / 1000);
            //console.log(ib.productdetails.p_live_start_time+"    current: "+ib.currentTime);
            if (ib.currentTime >= parseInt(ib.productdetails.p_live_start_time) && ib.currentTime <= parseInt(ib.productdetails.p_live_start_time) + liveSaleTime && ib.productdetails.p_is_frozen == 'N') {
                FirebaseService.getUpcomming(ib.productdetails.parent_category_id,6).then(function(data) {
                  ib.upcoming = data;
                });
                ib.timeRemaining = ib.productdetails.p_live_start_time - ib.currentTime + liveSaleTime;
                //if (ib.productdetails.p_on_live_sale == 'Y') {
                //ib.currentPrice = ib.productdetails.p_selling_price;
                //console.log('SUSU2');
                ib.onLiveSale = true;
                ib.startLiveSale();
                //}
                return true;
            } else {
                ib.upcoming = {};
                ib.onLiveSale = false;
                ib.stopLiveSale();
            }
            return false;

        };
        ib.checkLiveSale = $interval(ib.isOnLiveSale, 1000);
        ib.setThumb = function(thumb) {
            ib.mainThumbImage = thumb;
        };
        ib.addToCart = function(productId)
        {
            var data = {buyer_id: $localStorage.loginData.user_ID, product_id: ib.productdetails.p_ID, price: ib.productdetails.p_selling_price, product_title: ib.productdetails.p_title};
            ProductService.addToCart(data).success(function(response, status)
            {
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', false, 2500);
                    $state.go('app.cart');
                } else
                {
                    ionicToast.show(response.msg, 'top', false, 2500);
                }
            });
        }
        // A confirm dialog
        ib.confirmFreeze = function() {
            ib.freezingItem = true;
            ib.freezePrice = angular.copy(ib.currentPrice);
            ib.stopLiveSale(true);//send true to not update status in firebase
            ib.confirmPopup = $ionicPopup.confirm({
                title: 'Freezing item at: $' + ib.freezePrice,
                template: 'Are you sure you want to Freeze this Item?'
            });
            ib.confirmPopup.then(function(res) {
                if (res) {
                    //stop clock
                    //$state.go('app.payment');
                    ib.stopLiveSale();
                    //Freeze Item for all
                    //console.log($localStorage.loginData);
                    ib.freezeItem();
                } else {
                    //resume clock
                    ib.freezingItem = false;
                    //ib.startLiveSale();
                    //console.log('not sure');
                }
            });
        };
        ib.startLiveSale = function() {
            if (ib.liveSaleActive)
                return;
            ib.liveSaleActive = true;

            getPriceDecrement();
            ib.priceDropInterval = $interval(updatePrice, 1000);
            //ib.productdetails.p_selling_price--;
        };
        ib.stopLiveSale = function(noFirebaseUpdate) {
            //console.log('ib.liveSaleActive:'+ib.liveSaleActive)
            if (!ib.liveSaleActive)
                return;

            if (ib.confirmPopup) {
                ib.confirmPopup.close();
                //console.log('close called');
            }
            if (!noFirebaseUpdate) {
                ib.liveSaleActive = false;
                $interval.cancel(ib.priceDropInterval);
                ib.onLiveSale = false;
                FirebaseService.updateProduct(ib.productFirebaseKey, {p_on_live_sale: 'N'});
            }
        };
        var updatePrice = function() {
            //console.log(ib.currentPrice);
            if (ib.timeRemaining > 0) {
                ib.timeRemaining--;
                ib.currentPrice = parseFloat((ib.currentPrice - ib.eachDrop).toFixed(2));
                if(ib.currentPrice<ib.productdetails.p_min_selling_price)
                  ib.currentPrice = parseFloat(ib.productdetails.p_min_selling_price);
                ib.timeLeftPercent = ib.timeRemaining / liveSaleTime * 100;
                ib.currentSaving = ib.productdetails.p_max_selling_price - ib.currentPrice;
                ib.currentPercentOff = parseInt(ib.currentSaving/ib.productdetails.p_max_selling_price * 100);
            }
            else {
                //console.log('ended');
                ib.stopLiveSale();
            }
        };
        var getPriceDecrement = function() {
            var maxDrop = (ib.productdetails.p_selling_price - ib.productdetails.p_min_selling_price);
            //ib.timeRemaining;
            ib.eachDrop = (maxDrop / liveSaleTime);
            //console.log(ib.eachDrop);
            //console.log(ib.eachDrop+'   remaining: '+ib.timeRemaining);
            ib.currentPrice = (ib.productdetails.p_selling_price - ((liveSaleTime - ib.timeRemaining) * ib.eachDrop)).toFixed(2);
            //console.log(ib.currentPrice);
            //return eachDrop;
        };
    }
})();