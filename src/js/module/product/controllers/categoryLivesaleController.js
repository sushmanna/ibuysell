(function() {
  angular.module('ibuysell').controller('categoryLivesaleController', categoryLivesaleController);
  categoryLivesaleController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$stateParams', 'FirebaseService', 'ProductService', '$q', 'ionicToast', '$ionicLoading', '$ionicPopup', '$state', '$interval', 'UserService', '$localStorage'];
  function categoryLivesaleController($scope, $rootScope, $ionicModal, $timeout, $stateParams, FirebaseService, ProductService, $q, ionicToast, $ionicLoading, $ionicPopup, $state, $interval, UserService, $localStorage) {
    var ib = this;
    ib.message = '';
    $ionicModal.fromTemplateUrl('js/module/product/templates/presetModal.html', {
      scope: $scope
    }).then(function(modal) {
      ib.presetModal = modal;
    });
    ib.mainThumbImage = '';

    /*set message modal*/
    $ionicModal.fromTemplateUrl('js/module/product/templates/messageModal.html', {
      scope: $scope
    }).then(function(modal) {
      ib.messageModal = modal;
    });
    //close messageModal modal
    ib.closeMessageModal = function() {
      ib.messageModal.hide();
    };
    //open modal for messageModal
    ib.openMessageModal = function(index) {
      ib.message = '';
      ib.messageModal.show();
    };
    //send messageModal
    ib.submitMessage = function()
    {
      $ionicLoading.show({template: '<img src="img/loader.GIF">'});
      var data = {posted_to: ib.productdetails.p_uid, posted_by: $localStorage.loginData.user_ID, msg_content: ib.message, msg_type: 'message', msg_subject: ib.subject};
      UserService.sendMessageOrSupport(data).success(function(response, status)
      {
        $ionicLoading.hide();
        ib.messageModal.hide();
        if (status == 200) {
          ionicToast.show(response.msg, 'top', false, 2500);
        }
        else
        {
          ionicToast.show(response.msg, 'top', false, 2500);
        }
      });
    };

    ib.currentTime = Math.floor(Date.now() / 1000);
    //console.log(ib.currentTime);
    //ib.currentTime = 1449334820;
    ib.timeRemaining = 0;
    ib.onLiveSale = false;
    ib.openclass = 'cssSlideClose';
    ib.upcomming = {};
    ib.productdetails = {};
    $scope.$on('$ionicView.beforeEnter', function() {
      ib.catId = parseInt($stateParams.catId);
      //console.log('kkkkk');
      FirebaseService.getCategoryByID(ib.catId).then(function(response){
        ib.catDetails = response;
      });
     
      FirebaseService.getLivesaleCategoryProduct(ib.catId).then(function(response) {
        ib.catData = response[ib.catId];
        //console.log(ib.catDetails);
        //console.log(response);
        
        ib.initCategoryPage();
      });
      //api[1] = ProductService.getPresetCount(ib.productId, $rootScope.loginData.user_ID);

    });
//    $scope.$on('updatedProduct', function(event, args) {
//      ib.productdetails = args.value;
//      FirebaseService.getLiveSaleData().then(function(data) {
//        ib.upcoming = data.live[ib.catId]['upcoming'];
//      });
//      ib.isOnLiveSale();
//    });
    $scope.$on('livesaleCatUpdate', function(event, args) {
      //console.log(event);
      ib.upcoming = {};
      FirebaseService.getUpcomming(ib.catId,6).then(function(data) {
        ib.upcoming = data;
      });
//      if(ib.messageModal){
//        ib.closeMessageModal();
//      }

      ib.catData = args[ib.catId];
      ib.initCategoryPage();
    });
    $scope.$on('$ionicView.leave', function() {
      if (ib.productdetails.online_viewer <= 0)
      {
        var count = 0;
      }
      else
      {
        var count = parseInt(ib.productdetails.online_viewer - 1);
      }
      ib.stopLiveSale(true);
      //console.log('leave');
      $interval.cancel(ib.priceDropInterval);
      $interval.cancel(ib.checkLiveSale);
      FirebaseService.manageProductOnlineViewer(ib.productFirebaseKey, count);
    });
    
    ib.initCategoryPage = function(){
      $ionicLoading.show({template: '<img src="img/loader.GIF">'});
      ib.productdetails = {};
      ib.timeRemaining = 0;
      ib.currentPrice = 0;
      ib.timeLeftPercent = 0;
      ib.currentSaving = 0;
      ib.currentPercentOff = 0;
      if(ib.catData){
        var api = [];
        //console.log(response);
        
        ib.productId = ib.catData.p_ID;
        //console.log(ib.productId);
        ib.currentUserId = $localStorage.loginData.user_ID;
            var api = [];
            api[0] = FirebaseService.getProductById(ib.productId);
            api[1] = ProductService.getPresetCount(ib.productId, $localStorage.loginData.user_ID);
            $q.all(api)
                    .then(function(result) {
//                        ib.productFirebaseKey = result[0].key;
//                        ib.productdetails = result[0].value;
                        ib.productFirebaseKey = result[0][ib.productId].key;
                        ib.productdetails = result[0][ib.productId].value;
                        angular.forEach( productdata, function( value, key){
                          if( key != ib.productId)
                            delete productdata[key];
                        });
                        ib.tagcount = result[1].data.data.totalTag;
                        ib.tagcountaccess = result[1].data.data.userHasTag;
                       
                        if (ib.productdetails.img_url)
                        {
                            ib.mainThumbImage = ib.productdetails.img_url[0];
                        }
                        else {
                            ib.mainThumbImage = 'img/product-no-img.png';
                        }
                        ib.img_url = ib.productdetails.img_url;

                    })
                    .then(function() {
                        ib.isOnLiveSale();
                        var api1 = [];
                        //api1[0] = FirebaseService.getRelatedProduct(ib.productdetails.p_cat_category_id, ib.productdetails.p_ID); 
                        api1[0] = ProductService.getRelatedproductById(ib.productdetails.p_ID, $localStorage.loginData.user_ID);
                        api1[1] = UserService.getSellerById(ib.productdetails.p_uid);
                        $q.all(api1).then(function(response) {
                            $ionicLoading.hide();
                            //var data = _.sortBy(response[0], 'p_created').reverse();
                            //data = data.slice(0, 6);
                            ib.relatedProduct = response[0].data.data;
                            var rate = response[1].data.data.seller_rating;
                            var rateString = '';
                            if (rate > 0)
                            {
                                for (var i = 0; i < rate; i++)
                                {
                                    rateString += '<i class="ion-ios-star"></i>';
                                }
                            }

                            var description = (response[1].data.data.bio != '' && response[1].data.data.bio != null) ? '<p><b>Description: </b>' + response[1].data.data.bio + '</p>' : '';
                            var website = (response[1].data.data.website != '' && response[1].data.data.website != null) ? '<p><b>Website: </b>' + response[1].data.data.website + '</p>' : '';
                            var rating = (rateString != '') ? '<p><b>Rating: </b>' + rateString + '</p>' : '';
                            ib.data = [
                                {title: 'Short Description', description: ib.productdetails.p_description},
                                {title: 'Seller Info', description: '<p><b>Username: </b> <a href="#app/mycloset/' + ib.productdetails.p_uid + '">' + response[1].data.data.username + '</a></p>' + description + website + rating
                                },
                                {title: 'Delivery Option', description: (ib.productdetails.p_shipping_method.toLowerCase() == 'stamps_com' ? 'USPS' : ib.productdetails.p_shipping_method)}
                            ];
                            FirebaseService.manageProductOnlineViewer(ib.productFirebaseKey, parseInt(ib.productdetails.online_viewer + 1));
                            $ionicLoading.hide();
                        });
                    });
      } else {
        //ib.empty = 1;
        $ionicLoading.hide();
      }
    };
    
    ib.closeTagModal = function() {
      ib.presetModal.hide();
    };
    ib.openTagModal = function() {
      if (ib.productdetails.tagcountaccess)
      {
        ionicToast.show('You have already tag this product.', 'top', false, 2500);
      }
      else
      {
        ib.preset_price = 0;
        ib.presetModal.show();
      }
    };
    ib.submitPresetValue = function() {
      if (isNaN(parseFloat(ib.preset_price)) || parseFloat(ib.preset_price) <= 0)
      {
        ib.presetModal.hide();
        ionicToast.show('Please Enter valid Number.', 'top', false, 2500);
      }
      else
      {
        var data = {p_ID: ib.productId, user_id: $rootScope.loginData.user_ID, preset_value: ib.preset_price};
        ProductService.setPresetValue(data).success(function(response, status)
        {
          if (status == 200) {
            delete ib.preset_price;
            ib.presetModal.hide();
            ionicToast.show(response.msg, 'top', false, 2500);
            ib.tagcount = parseInt(ib.productdetails.tagcount) + 1;
            ib.productdetails.tagcountaccess = true;
          }
        });
      }
    };
    ib.freezeItem = function() {
      ib.currentTime = Math.floor(Date.now() / 1000);
      var data = {p_ID: ib.productId, p_is_frozen: 'Y', p_frozen_by: parseInt($rootScope.loginData.user_ID), p_frozen_price: parseFloat(ib.freezePrice), p_is_frozen:'Y', p_frozen_time: ib.currentTime, p_on_live_sale:'N'};
      FirebaseService.updateProduct(ib.productFirebaseKey, data);
      //console.log(ib.productdetails.parent_category_id);
      FirebaseService.updateLivesale(ib.productdetails.parent_category_id, data);
      ProductService.updateProduct(data).success(function(response, status) {
        if (status == 200) {
          
          //console.log(response.msg);
          //ib.presetModal.hide();
          ionicToast.show(response.msg, 'top', false, 5000);
          //ib.productdetails.tagcount = ib.productdetails.tagcount + 1;
        }
      });
    };
    ib.isOnLiveSale = function() {
      //console.log('SUSU');
      ib.currentTime = Math.floor(Date.now() / 1000);
      //console.log(ib.productdetails.p_live_start_time+"    current: "+ib.currentTime);
      if (ib.currentTime >= parseInt(ib.productdetails.p_live_start_time) && ib.currentTime <= parseInt(ib.productdetails.p_live_start_time) + liveSaleTime && ib.productdetails.p_is_frozen == 'N') {
        ib.timeRemaining = ib.productdetails.p_live_start_time - ib.currentTime + liveSaleTime;
        //if (ib.productdetails.p_on_live_sale == 'Y') {
        //ib.currentPrice = ib.productdetails.p_selling_price;
        //console.log('SUSU2');
        ib.onLiveSale = true;
        ib.startLiveSale();
        //}
        return true;
      } else {
        ib.onLiveSale = false;
        ib.stopLiveSale();
      }
      return false;

    };
    ib.checkLiveSale = $interval(ib.isOnLiveSale, 1000);
    ib.setThumb = function(thumb) {
      ib.mainThumbImage = thumb;
    };
    ib.addToCart = function(productId) {
      var data = {buyer_id: $rootScope.loginData.user_ID, product_id: ib.productdetails.p_ID, price: ib.freezePrice, product_title: ib.productdetails.p_title, is_frozen:1};
      ProductService.addToCart(data).success(function(response, status)
      {
        if (status == 200) {
          ionicToast.show(response.msg, 'top', false, 2500);
          $state.go('app.cart');
        } else
        {
          ionicToast.show(response.msg, 'top', false, 2500);
        }
      });
    }
    // A confirm dialog
    ib.confirmFreeze = function() {
      ib.freezingItem = true;
      ib.freezePrice = angular.copy(ib.currentPrice);
      ib.freezePercentOff = angular.copy(ib.currentPercentOff);
      
      ib.stopLiveSale(true);//send true to not update status in firebase
      ib.freezePopup = $ionicPopup.confirm({
        title: 'Freezing item at: $' + ib.freezePrice,
        template: 'Are you sure you want to Freeze this Item?'
      });
      ib.freezePopup.then(function(res) {
        if (res) {
          //stop clock
          $state.go('app.cart');
          ib.stopLiveSale();
          //Freeze Item for all
          //console.log($rootScope.loginData);
          ib.freezeItem();
          ib.addToCart(ib.productId);
          
        } else {
          //resume clock
          ib.freezingItem = false;
          //ib.startLiveSale();
          //console.log('not sure');
        }
      });
    };
    ib.startLiveSale = function() {
      if (ib.liveSaleActive)
        return;
      ib.liveSaleActive = true;

      getPriceDecrement();
      ib.priceDropInterval = $interval(updatePrice, 1000);
      //ib.productdetails.p_selling_price--;
    };
    ib.stopLiveSale = function(noFirebaseUpdate) {
      //console.log('ib.liveSaleActive:'+ib.liveSaleActive)
      if (!ib.liveSaleActive)
        return;

      if (ib.freezePopup) {
        ib.freezePopup.close();
        //console.log('close called');
      }
      if (!noFirebaseUpdate) {
        ib.liveSaleActive = false;
        $interval.cancel(ib.priceDropInterval);
        ib.onLiveSale = false;
        FirebaseService.updateProduct(ib.productFirebaseKey, {p_on_live_sale: 'N'});
      }
    };
    var updatePrice = function() {
      //console.log(ib.currentPrice);
      if (ib.timeRemaining > 0) {
        ib.timeRemaining--;
        ib.currentPrice = parseFloat((ib.currentPrice - ib.eachDrop).toFixed(2));
        if(ib.currentPrice<ib.productdetails.p_min_selling_price)
          ib.currentPrice = parseFloat(ib.productdetails.p_min_selling_price);
        ib.timeLeftPercent = ib.timeRemaining / liveSaleTime * 100;
        ib.currentSaving = parseFloat(ib.productdetails.p_max_selling_price) - ib.currentPrice;
        ib.currentPercentOff = parseInt(ib.currentSaving/ib.productdetails.p_max_selling_price * 100);
        //console.log('ib.currentPrice: '+ib.currentPrice+' ib.eachDrop:'+ib.eachDrop+' ib.currentPercentOff:'+ib.currentPercentOff);
      }
      else {
        //console.log('ended');
        ib.stopLiveSale();
      }
    };
    var getPriceDecrement = function() {
      var maxDrop = (ib.productdetails.p_selling_price - ib.productdetails.p_min_selling_price);
      //ib.timeRemaining;
      ib.eachDrop = parseFloat(maxDrop / liveSaleTime);
      //console.log(ib.eachDrop+'   remaining: '+ib.timeRemaining);
      ib.currentPrice = parseFloat((ib.productdetails.p_selling_price - ((liveSaleTime - ib.timeRemaining) * ib.eachDrop)).toFixed(2));
      //console.log(ib.currentPrice);
      //return eachDrop;
    };
    ib.submitPresetValue = function() {
            if (ib.tagcountaccess)
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Remove reminder',
                    template: 'Are you sure you remove this reminder?'
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                        var data = {user_id: $localStorage.loginData.user_ID, product_id: ib.productId};
                        ProductService.removePresetValue(data).success(function(response, status)
                        {
                            if (status == 200) {
                                $ionicLoading.hide();
                                ionicToast.show(response.msg, 'top', false, 2500);
                                ib.tagcountaccess = false;
                            }
                        });
                    }
                });
            }
            else
            {
                $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                var data = {p_ID: ib.productId, user_id: $localStorage.loginData.user_ID, preset_value: 0};
                ProductService.setPresetValue(data).success(function(response, status)
                {
                    if (status == 200) {
                        $ionicLoading.hide();
                        ionicToast.show(response.msg, 'top', false, 2500);
                        ib.tagcountaccess = true;
                    }
                });
            }
        };
  }
})();