(function() {
    angular.module('ibuysell').controller('checkoutController', checkoutController);
    checkoutController.$inject = ['$scope', '$state', '$rootScope', '$ionicModal', '$timeout', 'serverUrls', '$ionicLoading', 'ionicToast', '$location', 'ProductService', 'UserService', 'DataService', 'ShipstationService', '$localStorage', '$q'];

    function checkoutController($scope, $state, $rootScope, $ionicModal, $timeout, serverUrls, $ionicLoading, ionicToast, $location, ProductService, UserService, DataService, ShipstationService, $localStorage, $q) {
        var ib = this;
        // var ref = new Firebase("https://ibuysell.firebaseio.com/");
        //var ref1 = new Firebase("https://ibuysell.firebaseio.com/product");
        $scope.$on('$ionicView.beforeEnter', function(){
        	$ionicLoading.show({template: '<img src="img/loader.GIF">'});
        	//var address_data = {user_id: $localStorage.loginData.user_ID};
        	ib.cartproducts = [];
        	ib.cartaddress = [];
        	ib.totalCartAmount = 0;
        	ib.outIndex = 0;
        	ib.cartItemShipmentSelected = 0;
        	ib.openFirst = false;
        	ib.checkoutGroup = [{ id: 0, label: 'Checkout Items', data: []}, { id: 1, label: 'Shipping Address', data: []}, { id: 3, label: 'Billing Address', data: []}];
        	//ib.shipmentOptions = [{id: 'cubic~2', label: 'Cubic for $2'}, { id: 'flat_rate_envelope~3', label: 'Flat Rate Envelope for $3'}, {id: 'dvd_flat_rate_box~5', label: 'DVD Flat Rate Box for $5'}];

        	/*var api = [];
            api[0] = ProductService.cartProducts( data);
            api[1] = UserService.getAllAddress(address_data);*/
            ib.cartaddress = (DataService.getData()).address;
            
            var data = { buyer_id: $localStorage.loginData.user_ID, to_state: ib.cartaddress.s_state, to_country: ib.cartaddress.s_country, to_postal_code: ib.cartaddress.s_zip, to_city: ib.cartaddress.s_city};
            ShipstationService.getShipStationRates( data).success(function(response, status)
            //ProductService.cartProducts( data).success(function(response, status)
            {
                if(status == 200){
                	ib.cartproducts = response.data;                	
         	   		ib.checkoutGroup[0].data = response.data;
                }
             	
                angular.forEach( ib.cartaddress, function(value, key) {
                	var sp = key.split('_');
                	if( sp[0] == 's'){
                		ib.checkoutGroup[1].data.push({label: sp[1], value: value});
                	}
                	if( sp[0] == 'b'){
                		ib.checkoutGroup[2].data.push({label: sp[1], value: value});
                	}
				});
				$ionicLoading.hide();
            });


            /*$q.all(api).then(function(result){
            	ib.checkoutGroup[0].data = result[0].data.data;
            	angular.forEach(result[1].data.data, function(value, key) {
                	var sp = key.split('_');
                	if( sp[0] == 's'){
                		ib.checkoutGroup[1].data.push({label: sp[1], value: value});
                	}
                	if( sp[0] == 'b'){
                		ib.checkoutGroup[2].data.push({label: sp[1], value: value});
                	}
				});
                //ib.checkoutGroup[1].data = result[1].data.data;
         	   	ib.cartproducts = result[0].data.data;
         	   	$ionicLoading.hide();
            });*/
	  	});

	  	ib.calculateTotalCartAmount = function( chkgritem, cartitem){
	  		if( !ib.openFirst){	ib.toggleGroup( chkgritem); ib.openFirst = true; }
	  		if( chkgritem.id == 0){
	  			ib.totalCartAmount = ib.totalCartAmount + parseFloat("" + cartitem.crt_price.toFixed(2));
	  			if( !cartitem.shipping_method_spg_code) cartitem.shipping_method_spg_code = '';
	  			if( !cartitem.shipping_method_spg_price) cartitem.shipping_method_spg_price = 0;
	  			if( !cartitem.shipping_method_spg_combo) cartitem.shipping_method_spg_combo = '';
	  			if( !cartitem.crt_total_price) cartitem.crt_total_price = cartitem.crt_price;
	  		}
	  	};

	  	ib.chooseShippingService = function( cartitem){
            if( cartitem.shipping_method_spg_code == '') ib.cartItemShipmentSelected = ib.cartItemShipmentSelected + 1;
	  		ib.totalCartAmount = ib.totalCartAmount - parseFloat(cartitem.crt_total_price);
	  		var ship = cartitem.shipping_method_spg_combo.split('~');
	  		cartitem.shipping_method_spg_code = ship[0];
	  		cartitem.shipping_method_spg_price = ship[1];
	  		cartitem.crt_total_price = parseFloat(cartitem.crt_price) + parseFloat(cartitem.shipping_method_spg_price);
	  		ib.totalCartAmount = ib.totalCartAmount + parseFloat(cartitem.crt_total_price);
		};

	  	ib.toggleGroup = function(chkgritem) {
			if (ib.isGroupShown(chkgritem)) {
				ib.shownGroup = null;
			} else {
				ib.shownGroup = chkgritem;
			}
		};

		ib.isGroupShown = function(chkgritem) {
			return ib.shownGroup === chkgritem;
		};

        ib.choosePaymentMethod = function(){
            if( ib.cartItemShipmentSelected != ib.cartproducts.length){
        		ionicToast.show('Please choose all item shipment.', 'top', false, 2500);
        	} else {
	        	$ionicLoading.show({template: '<img src="img/loader.GIF">'});
	        	var data = { user_id: $localStorage.loginData.user_ID, ch_order_address: JSON.stringify(ib.cartaddress), cart_items: JSON.stringify(ib.cartproducts), ch_type: 'checkout'};
	        	ProductService.saveCartTempSave( data).success(function(response, status)
	            {
	                $ionicLoading.hide();
	                if(status == 200){ 
	                	var mdata = { address: (DataService.getData()).address, cart_temp_token: response.data, total_cart_amount: ib.totalCartAmount};
	                	DataService.setData( mdata);               	
	         	   		$state.go("app.payment");
	                }
	            });
            }
        };
    }
})();