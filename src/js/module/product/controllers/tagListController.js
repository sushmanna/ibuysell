(function() {
    angular.module('ibuysell').controller('tagListController', tagListController);
    tagListController.$inject = ['$scope', '$rootScope', '$localStorage', 'UserService', 'ProductService', 'ionicToast', '$ionicPopup','$ionicLoading'];
    function tagListController($scope, $rootScope, $localStorage, UserService, ProductService, ionicToast, $ionicPopup,$ionicLoading) {
        var ib = this;
        ib.tagList = {};
        $scope.$on('$ionicView.beforeEnter', function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            ib.notifications = {};
            UserService.getTagListByUser($localStorage.loginData.user_ID).success(function(response, status)
            {
                if (status == 200) {
                    ib.tagList = response.data;
                }
                $ionicLoading.hide();
            });
        });
        ib.removeReminder = function(index)
        {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Remove reminder',
                template: 'Are you sure you remove this reminder?'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                    var data = {user_id: $localStorage.loginData.user_ID, product_id: index};
                    ProductService.removePresetValue(data).success(function(response, status)
                    {
                        if (status == 200) {
                            ionicToast.show(response.msg, 'top', false, 2500);
                            UserService.getTagListByUser($localStorage.loginData.user_ID).success(function(response, status)
                            {
                                if (status == 200) {
                                    $ionicLoading.hide();
                                    ib.tagList = response.data;
                                }
                            });
                        }
                    });
                } else {
                    // console.log('You are not sure');
                }
            });

        };
    }
})();