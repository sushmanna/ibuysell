(function() {
    angular.module('ibuysell').controller('cartController', cartController);
    cartController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', 'serverUrls', '$ionicLoading', 'ionicToast', '$location', 'ProductService', 'UserService', 'DataService', '$localStorage', '$q', '$filter', '$interval'];

    function cartController($scope, $rootScope, $ionicModal, $timeout, serverUrls, $ionicLoading, ionicToast, $location, ProductService, UserService, DataService, $localStorage, $q, $filter, $interval) {
        var ib = this;

        $scope.$on('$ionicView.beforeEnter', function(){
            ib.showCartItem = true;
        	$ionicLoading.show({template: '<img src="img/loader.GIF">'});
        	var data = { buyer_id: $localStorage.loginData.user_ID};
            var address_data = {user_id: $localStorage.loginData.user_ID};
        	ib.cartproducts = [];
            ib.address = [];
        	ib.totalCartAmount = 0;
            ib.addressMissing = 1;
            ib.countrylist = {};

            DataService.setData({});

            var api = [];
            api[0] = ProductService.cartProducts( data);
            api[1] = UserService.getAllAddress(address_data);
            api[2] = UserService.countrylist();

            $q.all(api).then(function(result){
                ib.cartproducts = result[0].data.data;
                
                ib.startTimer(); //for freezed products
                ib.address = result[1].data.data;
                ib.countrylist = result[2].data.data.country;
                if( ib.address.s_address1) ib.addressMissing = 0;
                $ionicLoading.hide();
            });
	  	});

        ib.calculateTotalCartAmount = function( cartitem){
            ib.totalCartAmount = ib.totalCartAmount + parseFloat("" + cartitem.crt_price.toFixed(2));
        };

	  	ib.removeCartItem = function( item){
        $ionicLoading.show({template: '<img src="img/loader.GIF">'});
	  		var data = { crt_buyer_id : $rootScope.loginData.user_ID, crt_id : item.crt_id};
	  		ProductService.removeCartItem( data).success(function(response, status)
            {
            	if(status == 200){
            		ionicToast.show(response.msg, 'top', false, 2500);
            		ib.cartproducts = response.data;
                    ib.totalCartAmount = ib.totalCartAmount - parseFloat("" + item.crt_price.toFixed(2));
         	   	}
         	   	$ionicLoading.hide();
            });
	  	};

        ib.checkAddress = function(){
            ib.showCartItem = false;
        };

        ib.confirmAddress = function(){
            if( ib.address.s_name === null || ib.address.s_name.length == 0){
                ionicToast.show('Missing shipping full name.', 'top', false, 2500);
            }
            else if( ib.address.s_address1 == null || ib.address.s_address1.length == 0){
                ionicToast.show('Missing shipping address 1.', 'top', false, 2500);
            }
            else if( ib.address.s_city == null || ib.address.s_city.length == 0){
                ionicToast.show('Missing shipping city.', 'top', false, 2500);
            }
            else if( ib.address.s_state == null || ib.address.s_state.length == 0){
                ionicToast.show('Missing shipping state.', 'top', false, 2500);
            }
            else if( ib.address.s_country === null || ib.address.s_country.length == 0){
                ionicToast.show('Missing shipping country.', 'top', false, 2500);
            }
            else if( ib.address.s_zip === null || ib.address.s_zip.length == 0){
                ionicToast.show('Missing shipping zip.', 'top', false, 2500);
            }
            else if( !ib.address.checkBoxValue && (ib.address.b_name === null || ib.address.b_name.length == 0)){
                ionicToast.show('Missing billing full name.', 'top', false, 2500);
            }
            else if( !ib.address.checkBoxValue && (ib.address.b_address1 === null || ib.address.b_address1.length == 0)){
                ionicToast.show('Missing billing address 1.', 'top', false, 2500);
            }
            else if( !ib.address.checkBoxValue && (ib.address.b_city === null || ib.address.b_city.length == 0)){
                ionicToast.show('Missing billing city.', 'top', false, 2500);
            }
            else if( !ib.address.checkBoxValue && (ib.address.b_state === null || ib.address.b_state.length == 0)){
                ionicToast.show('Missing billing state.', 'top', false, 2500);
            }
            else if( !ib.address.checkBoxValue && (ib.address.b_country === null || ib.address.b_country.length == 0)){
                ionicToast.show('Missing billing country.', 'top', false, 2500);
            }
            else if( !ib.address.checkBoxValue && (ib.address.b_zip === null || ib.address.b_zip.length == 0)){
                ionicToast.show('Missing billing zip.', 'top', false, 2500);
            }
            else{
                if( ib.address.checkBoxValue){
                    ib.address.b_name = ib.address.s_name;
                    ib.address.b_address1 = ib.address.s_address1;
                    ib.address.b_address2 = ib.address.s_address2;
                    ib.address.b_city = ib.address.s_city;
                    ib.address.b_state = ib.address.s_state;
                    ib.address.b_country = ib.address.s_country;
                    ib.address.b_zip = ib.address.s_zip;
                    ib.address.b_phone = ib.address.s_phone;
                }

                if( ib.addressMissing){
                    $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                    ib.address.user_id = $localStorage.loginData.user_ID;
                    UserService.updateAddress(ib.address).success(function(response, status)
                    {
                        $ionicLoading.hide();
                        if (status == 200) {
                            ionicToast.show(response.msg, 'top', false, 2500);
                            DataService.setData( {address: ib.address});
                            $location.path("/app/checkout");
                        } else
                        {
                            ionicToast.show(response.msg, 'top', false, 2500);
                        }
                    });
                } else {
                    DataService.setData( {address: ib.address});
                    $location.path("/app/checkout");
                }
            }
        };
        
        $scope.$on('$ionicView.leave', function() {
          ib.stopTimer();
        });
        
        ib.startTimer = function(){
          ib.cartTimer = $interval(ib.timerInterval,1000);
//          angular.forEach(ib.cartproducts,function(value, key){
//            if(value.crt_is_frozen){
//              ib.timerInterval(key);
//            }
//          });
        };
        ib.stopTimer = function(){
          //$interval.cancel(ib.priceDropInterval);
          ib.cartTimer = null;
        };
        ib.timerInterval = function(){
          ib.currentTime = (Math.floor(Date.now() / 1000));
          angular.forEach(ib.cartproducts,function(value, key){
            if(value.crt_is_frozen) {
              if(value.crt_product_added+freezeTime>ib.currentTime){
                ib.cartproducts[key].showTimer = 1;
                ib.cartproducts[key].timeRemaining = value.crt_product_added + freezeTime - ib.currentTime;
              } else {
                ib.removeCartItem(ib.cartproducts[key]);
                ib.cartproducts[key].showTimer = 0;
              }
            }
          });
        };
    }
})();