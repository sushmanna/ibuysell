(function() {
    angular.module('ibuysell').controller('categoryallController', categoryallController);
    categoryallController.$inject = ['$scope', '$rootScope', '$ionicModal', '$ionicPopup', '$timeout', '$ionicPlatform', '$ionicHistory', 'FirebaseService','$ionicLoading','$ionicScrollDelegate'];

    function categoryallController($scope, $rootScope, $ionicModal, $ionicPopup, $timeout, $ionicPlatform, $ionicHistory, FirebaseService,$ionicLoading, $ionicScrollDelegate) {
        var ib = this;
        //ib.catdata={};
        $scope.$on('$ionicView.beforeEnter', function() {
           $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            FirebaseService.getProductfromfirebase().then(function(data) {
//              data = _.sortBy(data, 'p_livetime').reverse();
                var data = _.filter(data, function(obj) {
                    if (obj.p_is_frozen == 'N' && obj.p_on_live_sale == 'N' && obj.p_is_sold=='N') {
                        return true;
                    }
                });
                data = _.groupBy(data, 'parent_category_name');
                data = _.mapObject(data, function(group) {
                    return group.slice(0, 6);
                });
                ib.catdata = data;
                $ionicLoading.hide();
                $ionicScrollDelegate.scrollTop(true);
            });
        });
    }
})();