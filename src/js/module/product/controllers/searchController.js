(function() {
    angular.module('ibuysell').controller('searchController', searchController);
    searchController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', 'ProductService', 'FirebaseService'];
    function searchController($scope, $rootScope, $ionicModal, $timeout, ProductService, FirebaseService) {
        var ib = this;
        ib.totalList = [];
        $scope.$on('$ionicView.enter', function() {
            ib.dropdownlists = [];
            ib.keyword = "";
            ib.searchpage = true;
            ib.list = [];
            ib.counter = 0;
            ib.totalList = [];
        });
        ib.callbackMethod = function()
        {
            FirebaseService.searchKeyword(ib.keyword, 'keyword').then(function(response) {
                ib.dropdownlists = response;
            });
        };
        ib.setkeyword = function(key)
        {
            ib.dropdownlists = [];
            ib.keyword = key;
            ib.searchpage = false;
            ib.counter = 0;
            ib.totalList = [];
            ib.list = [];
            FirebaseService.searchKeyword(ib.keyword, 'object').then(function(response) {
                ib.totalList = response;
                ib.populateList();
            });
        };
//
        ib.populateList = function() {
            for (var i = 0; i <= 3; i++) {
                if (ib.totalList[ib.counter] != null)
                {
                    if(ib.totalList[ib.counter].p_selling_price){
                        ib.list.push(ib.totalList[ib.counter]);
                        ib.counter++;
                    }
                }
            }
            //$scope.$apply(ib.list);
            $scope.$broadcast('scroll.infiniteScrollComplete');
        }

        ib.canWeLoadMoreContent = function() {
            return (ib.counter > ib.totalList.length) ? false : true;
        }
    }
})();