(function() {
    angular.module('ibuysell').controller('uploadProductController', uploadProductController);

    uploadProductController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', 'ProductService', 'ShipstationService', '$cordovaImagePicker', 'ionicToast', '$q', '$ionicLoading', '$cordovaCamera', '$location', 'UserService', '$localStorage','$cordovaDevice','$ionicPopup'];
    function uploadProductController($scope, $rootScope, $ionicModal, $timeout, ProductService, ShipstationService, $cordovaImagePicker, ionicToast, $q, $ionicLoading, $cordovaCamera, $location, UserService, $localStorage,$cordovaDevice,$ionicPopup) {
        var ib = this;
        var maxsaleper = 0;
        var minsaleper = 0;
        $scope.$on('$ionicView.beforeEnter', function() {
            ib.productData = {};
            ib.productimg = [];
            maxsaleper = 0;
            minsaleper = 0;
//            if (window.cordova) {
//                if ($cordovaDevice.getPlatform().toLowerCase() == 'android' || $cordovaDevice.getPlatform().toLowerCase() == 'ios') {
//                    ib.openCamera();
//                }
//            }
//            var data = {user_id: $localStorage.loginData.user_ID};
//            UserService.checkverify(data).success(function(response, status){
//                if (status == 200)
//                {
//                    if (response.data) {
//                        ib.openCamera();
//                    }
//                    else
//                    {
//                        ionicToast.show('You have to subscribe before upload product.', 'top', false, 3000);
//                        $location.path("/app/subscription");
//                    }
//                }
//            });
        });
        $scope.$on('$ionicView.enter', function() {
            var process = [];
            //get setting
            process[0] = ProductService.getSettings();
            //get category 
            process[1] = ProductService.getCategories(ib.logindata);
            //get size and brand 
            process[2] = ProductService.getSizeBrand(); 
            //get item condition 
            process[3] = ProductService.getItemCondition();
            $q.all(process)
                    .then(function(result) {
                        if (result[0].status == 200) {
                            maxsaleper = parseFloat(result[0].data.data['Product Min Selling price(% of Selling Price)']);
                            minsaleper = parseFloat(result[0].data['Product Selling price(% of MSRP)']);
                        }
                        if (result[1].status == 200) {
                            var cc = [];
                            ib.categoryarr = result[1].data.data.allCategories;
                            for (cat in result[1].data.data.allCategories) {
                                cc.push(cat);
                            }
                            ib.categories = cc;
                            ib.mcategories = result[1].data.data.mainCategories;
                        }
                        if (result[2].status == 200) {
                            ib.size = result[2].data.data.size;
                            ib.brands = result[2].data.data.brands;
                        }
                        if (result[3].status == 200) {
                            ib.procondition = result[3].data.data;
                        }
                    })
                    .then(function() {
                        ShipstationService.getShipStationCarriers().success(function(response, status){
                            if (status == 200)
                            {
                                ib.shipMethods = response.data;
                            }
                        });
                        /*ib.shipMethods = [
                            {"name": "Express 1", "code": "express_1", "accountNumber": "fe71c33f", "requiresFundedAccount": true, "balance": 0.27},
                            {"name": "Stamps.com", "code": "stamps_com", "accountNumber": "SS123", "requiresFundedAccount": true, "balance": 24.14},
                            {"name": "UPS", "code": "ups", "accountNumber": "ABCR80", "requiresFundedAccount": false, "balance": 0},
                            {"name": "FedEx", "code": "fedex", "accountNumber": "297929999", "requiresFundedAccount": false, "balance": 0},
                            {"name": "Endicia", "code": "endicia", "accountNumber": "913999", "requiresFundedAccount": true, "balance": 84.86}
                        ];*/
                    });
            ib.productimg = [];
            ib.setImages();
        });
        /*accordion slider*/
        ib.group = {title: false, desctiption: false, image: false};
        ib.groupClick = function(g) {

            if (ib.group[g] === true)
            {
                ib.group = {title: false, desctiption: false, image: false};
            } else
            {
                ib.group = {title: false, desctiption: false, image: false};
                ib.group[g] = true;
            }
        };

        /*image action*/
        ib.imageAction = function(action) {
            var remainImageCount = 4 - ib.productimg.length;
            if (ib.productimg.length < 4)
            {
                if (action === 'camera')
                {
                    ib.openCamera();
                }
                else if (action === 'gallery')
                {
                    ib.openGallery(remainImageCount);
                }
            } else
            {
                ionicToast.show('Error: Maxmum 4 images is selected.', 'middle', false, 2500);
            }
        };
        /*open camera*/
        ib.openCamera = function() {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            var cameraOptions = {
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                targetWidth: 400,
                targetHeight: 400
            };
            $cordovaCamera.getPicture(cameraOptions).then(function(imageData) {
                ib.productimg.push(imageData);
                ib.setImages();
            }, function(err) {
                // error
                ionicToast.show('Error:' + err, 'middle', false, 2500);
            }).then(function() {
                $ionicLoading.hide();
            });
        };
        /*open gallery*/
        ib.openGallery = function(count) {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            var options = {
                maximumImagesCount: count,
                width: 400,
                height: 400,
                quality: 100
            };
            $cordovaImagePicker.getPictures(options)
                    .then(function(results) {
                        for (var i = 0; i < results.length; i++) {
                            ib.productimg.push(results[i]);
                        }
                        ib.setImages();
                    }, function(error) {
                        ionicToast.show('Upload image failed.Please Select image again', 'top', false, 3000);
                    })
                    .then(function() {
                        $ionicLoading.hide();
                    });
        };
        /*manage Image*/
        ib.removeImage = function(index) {
            ib.productimg.splice(index, 1);
            ib.setImages();
        };
        /*set image*/
        ib.setImages = function() {
            if (ib.productimg[0]) {
                ib.first = ib.productimg[0];
            } else {
                ib.first = 'img/upload-no-img.png';
            }
            if (ib.productimg[1]) {
                ib.second = ib.productimg[1];
            } else {
                ib.second = 'img/upload-no-img.png';
            }
            if (ib.productimg[2]) {
                ib.third = ib.productimg[2];
            } else {
                ib.third = 'img/upload-no-img.png';
            }
            if (ib.productimg[3]) {
                ib.forth = ib.productimg[3];
            } else {
                ib.forth = 'img/upload-no-img.png';
            }
            $scope.$apply();
        };
        ib.showInfo = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Proof of Authenticity Details',
                template: 'Please enter the style number, serial number, model number, date code or item number'
            });
            alertPopup.then(function(res) {
               
            });
        };
        /*time picker*/
        ib.timePickerObject = {
            inputEpochTime: ((new Date()).getHours() * 60 * 60), //Optional
            step: 5, //Optional
            format: 12, //Optional
            titleLabel: '12-hour Format', //Optional
            setLabel: 'Set', //Optional
            closeLabel: 'Close', //Optional
            setButtonType: 'button-positive', //Optional
            closeButtonType: 'button-stable', //Optional
            callback: function(val) {    //Mandatory
                timePickerCallback(val);
            }
        };
        function timePickerCallback(val) {
            if (typeof (val) === 'undefined') {
                // console.log('Time not selected');
            } else {
                var selectedTime = new Date(val * 1000);
                ib.productData.liveSaleTime = selectedTime.getUTCHours() + ':' + selectedTime.getUTCMinutes();
                // console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
            }
        }
        function IsNumeric(input) {
            return (input - 0) == input && ('' + input).trim().length > 0;
        }
        /*upload photo*/
        ib.uploadProduct = function(productData) {
            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
            var validCheck = true;
            var checksale;
            var checkminsale;

            //blank check
            if (ib.productimg.length <= 0)
            {
                validCheck = false;
                ionicToast.show('Please select atleast one image.', 'top', false, 2500);
                $ionicLoading.hide();
            }
            else if (!productData.p_title)
            {
                validCheck = false;
                ionicToast.show('Please Enter Title.', 'top', false, 2500);
                $ionicLoading.hide();
            }
//            else if (!productData.liveSaleDate)
//            {
//                validCheck = false;
//                ionicToast.show('Please Enter LiveSale Date.', 'top', false, 2500);
//                $ionicLoading.hide();
//            }
//            else if (!productData.liveSaleTime)
//            {
//                validCheck = false;
//                ionicToast.show('Please Enter LiveSale Time', 'top', false, 2500);
//                $ionicLoading.hide();
//            }
            else if (!productData.p_description)
            {
                validCheck = false;
                ionicToast.show('Please Enter Description.', 'top', false, 2500);
                $ionicLoading.hide();
            }
            else if (!productData.p_max_selling_price || !IsNumeric(productData.p_max_selling_price))
            {
                validCheck = false;
                ionicToast.show('Please Enter valid Max sale Price .', 'top', false, 2500);
                $ionicLoading.hide();
            }
            else if (!productData.p_weight || !IsNumeric(productData.p_weight))
            {
                validCheck = false;
                ionicToast.show('Please Enter valid weight .', 'top', false, 2500);
                $ionicLoading.hide();
            }
            else if (!productData.p_selling_price || !IsNumeric(productData.p_selling_price))
            {
                validCheck = false;
                ionicToast.show('Please Enter valid Sale Price.', 'top', false, 2500);
                $ionicLoading.hide();
            }
            else if (!productData.p_min_selling_price || !IsNumeric(productData.p_selling_price))
            {
                validCheck = false;
                ionicToast.show('Please Enter valid Min sale Price.', 'top', false, 2500);
                $ionicLoading.hide();
            }
            else if (!productData.p_shipping_method)
            {
                validCheck = false;
                ionicToast.show('Please Select Shipping Method.', 'top', false, 2500);
                $ionicLoading.hide();
            }
            /*else
            {
                //check sale price
                checksale = parseInt(parseFloat(productData.p_max_selling_price * maxsaleper) / 100);
                checkminsale = parseInt(parseFloat(checksale * minsaleper) / 100);
                if (checksale < productData.p_selling_price)
                {
                    validCheck = false;
                    ionicToast.show('Sale Price is Maximum up to ' + checksale + '.', 'top', false, 2500);
                    $ionicLoading.hide();
                }
                else if (productData.p_min_selling_price >= checksale || productData.p_min_selling_price < checkminsale) {
                    validCheck = false;
                    ionicToast.show('Min Sale Price is ' + (checksale - 1) + ' to ' + checkminsale + ' allowed.', 'top', false, 2500);
                    $ionicLoading.hide();
                }
            }*/

            if (validCheck)
            {
                productData.p_uid = $rootScope.loginData.user_ID;
                productData.pcat = ib.mcategories[productData.category];
                ProductService.uploadProduct(productData).success(function(response, status) {
                    if (status == 200) {
                        var process = [];
                        for (var i = 0; i < ib.productimg.length; i++) {
                            var imageElement = {};
                            imageElement = {product_id: response.data.productId, imageUrl: ib.productimg[i], counter: i};
                            process[i] = ProductService.uploadProductImage(imageElement);
                        }
                        var data = {user_id: $localStorage.loginData.user_ID};
                        process[i+1]=UserService.checkverify(data);
                        $q.all(process).then(function(res) {
                            $ionicLoading.hide();
                            ib.productimg = [];
                            ib.productData = {};
                            ib.setImages();
                            if (res[i+1].data.data) {
                                ionicToast.show(response.msg, 'top', false, 3000);
                                $location.path("/app/mycloset/");
                            }
                            else
                            {
                                ionicToast.show('You have to subscribe before upload product.', 'top', false, 3000);
                                $location.path("/app/subscription");
                            }
                        });
                    } else
                    {
                        $ionicLoading.hide();
                        ionicToast.show(response.msg, 'top', false, 2500);
                    }
                });
            }
        };

        ib.chooseBrand = function( brand){
            ib.productData.p_brand = brand;
            ib.show_brand = false;
        };

        ib.checkBrandList = function(){
            if( ib.productData.p_brand) ib.show_brand = true;
            else ib.show_brand = false;
        };
    }
})();