
(function() {
    angular.module('ibuysell')
            .filter('imagecheck', imagecheck)
            .filter('showrate', showrate)
            .filter('numbercheck', numbercheck)
            .filter('showonlyrate', showonlyrate)
            .filter('showMinSec', showMinSec)
            .filter('isEmpty', isEmpty);

    function imagecheck() {
        filter.$stateful = true;
        return filter;
        function filter(input) {
            if (input)
            {
                return input;
            }
            else
            {
                return 'img/product-no-img.png';
            }
        }
    } 
    function isEmpty() {
        filter.$stateful = true;
        return filter;
        function filter(input) {
           if(angular.equals({}, input))
           {
               return angular.equals({}, input);
           }
           else if(input ==null)
           {
               return true;
           }
        }
    }
    function numbercheck() {
        filter.$stateful = true;
        return filter;
        function filter(input) {
            return Math.abs(input);
        }
    }
    function showrate() {
        filter.$stateful = true;
        return filter;
        function filter(input) {
            var rateString = '';
            if (input > 0)
            {
                rateString = 'Seller Rating: ';
                for (var i = 0; i < input; i++)
                {
                    rateString += '<i class="ion-ios-star"></i>';
                }
            }
            return rateString;
        }
    }
    function showonlyrate() {
        filter.$stateful = true;
        return filter;
        function filter(input) {
            var rateString = '';
            if (input > 0)
            {
                rateString = '';
                for (var i = 0; i < input; i++)
                {
                    rateString += '<i class="ion-ios-star"></i>';
                }
            }
            return rateString;
        }
    }
    
    function showMinSec() {
        filter.$stateful = true;
        return filter;
        function filter(input) {
            
            var time = '';
            var minute = parseInt(input/60);
            var secs = parseInt(input%60);
            
            var minute_text = minute>1?'mins ':'min ';
            var seconds_text = secs>1?'secs':'sec';
            
            time = minute>0?minute+' '+minute_text:'';
            time += secs+' '+seconds_text;
            
            return time;
        }
    }

})();