(function() {
    angular.module('ibuysell').factory('ShipstationService', ShipstationService);
    ShipstationService.$inject = ['$http', 'serverUrls', '$window', '$location', '$rootScope','$q'];
    function ShipstationService($http, serverUrls, $window, $location, $rootScope,$q) {
		var data = {};  

        data.getShipStationCarriers = function() {
            return $http({
                url: serverUrls.getshipstationcarriers,
                method: 'GET',
                responseText: 'JSON'
            });
        };

        data.getShipStationRates = function(data) {
            return $http({
                url: serverUrls.getshipstationrates,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };

        return data;
    }
})();
