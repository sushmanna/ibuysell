(function() {
    angular.module('ibuysell').factory('NotificationService', NotificationService);
    NotificationService.$inject = ['$http', 'serverUrls', '$window', '$location', '$rootScope', '$cordovaLocalNotification', 'UserService', '$interval', '$ionicPopup'];
    function NotificationService($http, serverUrls, $window, $location, $rootScope, $cordovaLocalNotification, UserService, $interval, $ionicPopup) {
        var data = {};
        data.getMessageNotifications = function(userId) {
            $interval(function() {
                data.getMessageNotificationCount(userId);
            }, 30000);
        };
        data.getMessageNotificationCount = function(userId) {
            var data = {user_id: userId};
            UserService.messageNotification(data).success(function(response, status)
            {
                if (status == 200) {
                    var count = response.data;
                    if (count > 0)
                    {
                        $rootScope.loginData.MessageCount = count;
                        $rootScope.$broadcast('MessageCount', {MessageCount: count});
                    }
                }
            });
        };
        data.getAlertNotifications = function(userId) {
            $interval(function() {
                data.getAlertNotificationCount(userId);
            }, 30000);
        };
        data.getAlertNotificationCount = function(userId) {
            var data = {user_id: userId};
            UserService.alertNotification(data).success(function(response, status)
            {
                if (status == 200) {
                    var count = response.data.count;
                    if (count > 0)
                    {
                        if (response.data.alert_type == 'alert')
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: response.data.title,
                                template: response.data.template
                            });
                            alertPopup.then(function(res) {

                            });
                        }
                        else if (response.data.alert_type == 'confirm')
                        {
                            var confirmPopup = $ionicPopup.confirm({
                                title: response.data.title,
                                template: response.data.template,
                                buttons: [
                                    {text: 'Cancel'},
                                    {
                                        text: '<b>'+response.data.confirmbtm_text+'</b>',
                                        type: 'button-positive',
                                        onTap: function(res) {
                                            if (res) {
                                                if (response.data.location) {
                                                    $location.path(response.data.location);
                                                }
                                            }
                                        }
                                    }
                                ]
                            });
                            confirmPopup.then(function(res) {
                            });
                        }
                    }
                }
            });
        };
        data.getLocalNotifications = function(userId) {
            $interval(function() {
                data.getLocalNotificationCount(userId);
            }, 30000);
        };
        data.getLocalNotificationCount = function(userId) {
            var data = {user_id: userId};
            UserService.localNotification(data).success(function(response, status)
            {
                if (status == 200) {
                    var count = response.data;
                    if (count > 0)
                    {
                        UserService.getLocalNotification(data).success(function(response, status)
                        {
                            if (status == 200) {
                                for (var m = 0; m < response.data.length; m++)
                                {
                                    $cordovaLocalNotification.schedule({
                                        id: response.data[m].notification_ID + ' ' + response.data[m].notification_create_time + ' ' + userId,
                                        title: null,
                                        text: response.data[m].notification_details,
                                        sound: "sound/notification.mp3",
                                    });
                                }
                            }
                        });
                    }
                }
            });
        };
        return data;
    }
})();
