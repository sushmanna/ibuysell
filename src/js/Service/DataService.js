/*(function() {
    angular.module('ibuysell').factory('$data', DataService);
    DataService.$inject = ['$http'];
    function DataService($http) {
        var data = {};
        return data;
    }
})();*/
(function() {
    angular.module('ibuysell').service('DataService', DataService);
    DataService.$inject = ['$http'];
    function DataService($http) {
        var dc = this;
        dc.cartdata = {};

        return{
			setData : setData,
			getData : getData		
		};

        function setData( paramData){
			dc.cartdata = paramData;
		};

		function getData(){
			return dc.cartdata;
		};
    }
})();




