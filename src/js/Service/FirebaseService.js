(function() {
    angular.module('ibuysell').factory('FirebaseService', FirebaseService);
    FirebaseService.$inject = ['$rootScope', '$http', 'serverUrls', '$window', '$q', '$firebaseObject', '$firebaseArray', '$firebaseAuth'];
    function FirebaseService($rootScope, $http, serverUrls, $window, $q, $firebaseObject, $firebaseArray, $firebaseAuth) {
        var data = {};
       // var firebase = new Firebase(serverUrls.firebaseLink);
        var category = new Firebase(serverUrls.firebaseCategory);
       // var categoryArray = $firebaseArray(category);
        var product = new Firebase(serverUrls.firebaseProduct);
        var livesale = new Firebase(serverUrls.firebaseLivesale);
        //var productArray = $firebaseArray(product);
        //var productObject = $firebaseObject(product);

        //get product array from fire base table
//        function getFirebaseArray(obj) {
//            var deferred = $q.defer();
//            obj.once("value", function(data) {
//                var productdata = []
//                data.forEach(function(value) {
//                    var childData = value.val();
//                    productdata.push(childData);
//                });
//                deferred.resolve(productdata);
//            });
//            return deferred.promise;
//        }
        //get product table in firebase
        data.getProductfromfirebase = function() {
            var deferred = $q.defer();
            product.on("value", function(data) {
                var productdata = []
                data.forEach(function(value) {
                    var key = value.key();
                    var childData = value.val();
                    childData.firebasekey = key;
                    if(childData.img_url)
                    {
                        childData.thumbimage = childData.img_url[0];
                    }
                    else{
                        childData.thumbimage = 'img/product-no-img.png';
                    }
                    productdata.push(childData);
                });
                deferred.resolve(productdata);
            });
            return deferred.promise;
        };
        //get profuct by firebase id
        data.getProductById = function(FireId) {
            var deferred = $q.defer();
            product.orderByChild("p_ID").equalTo(parseInt(FireId)).on("value", function(data) {
                productdata[FireId] = {};
                for (key in data.val())
                {
                    productdata[FireId].key = key;
                }
                var s = data.val();
                productdata[FireId].value = s[productdata[FireId].key];
                //console.log( productdata);
                $rootScope.$broadcast('updatedProduct', productdata);
                deferred.resolve(productdata);
            });
            return deferred.promise;
        };
        data.manageProductOnlineViewer = function(FireId, counter) {
            if (FireId) {
                var prodRef = new Firebase(serverUrls.firebaseProduct + '/' + FireId);
                prodRef.update({online_viewer: counter});
            }
        };
        data.getRelatedProduct = function(categoryId,productId) {
            var deferred = $q.defer();
            product.orderByChild("p_cat_category_id").equalTo(categoryId).on("value", function(data) {
                var valll = {}
                for (key in data.val())
                {
                    if(data.val()[key].p_ID != productId)
                    {
                        valll[key]=data.val()[key];
                    }
                }
                deferred.resolve(valll);
            });
            return deferred.promise;
        };
//        data.getLiveSaleData = function(categoryId) {
//            var deferred = $q.defer();
//            product.orderByChild("p_cat_category_id").equalTo(categoryId).on("value", function(data) {
//                  deferred.resolve(data.val());
//            });
//            return deferred.promise;
//        }
//        data.getLiveSaleData = function() {
//            var deferred = $q.defer();
//            product.on("value", function(data) {
//                var productdata = data.val();
//                $rootScope.$broadcast('updateLiveSaleList', productdata);
//                deferred.resolve(productdata);
//            });
//            return deferred.promise;
//        };
        data.getLiveSaleData = function() {
            var deferred = $q.defer();
            livesale.on("value", function(data) {
                var livesaledata = data.val();
                $rootScope.$broadcast('updateLiveSaleList', livesaledata);
                deferred.resolve(livesaledata);
            });
            return deferred.promise;
        };
        
        data.getUpcomming = function(cID,limit) {
            var deferred = $q.defer();
            var liveRef1 = new Firebase(serverUrls.firebaseLivesale + '/live/'+cID+'/upcoming');
            liveRef1.limitToFirst(limit).on("value", function(data) {
                var upcomingdata = data.val();
                $rootScope.$broadcast('updateUpcoming', upcomingdata);
                deferred.resolve(upcomingdata);
            });
            return deferred.promise;
        };
        
        data.getLiveSaleDataByProduct = function() {
            var deferred = $q.defer();
            product.orderByChild("p_on_live_sale").equalTo('Y').on("value", function(data) {
                var livesaledata = data.val();
                //console.log(livesaledata);
                var currentTime = (Math.floor(Date.now() / 1000));
                angular.forEach(livesaledata,function(value, key){
                  if(value.p_is_frozen=='Y' || (parseInt(value.p_live_start_time) <= currentTime &&  (parseInt(value.p_live_start_time)+liveSaleTime) <= currentTime)){
                    delete livesaledata[key];
                  }
                });
                $rootScope.$broadcast('updateLiveSaleProductList', livesaledata);
                deferred.resolve(livesaledata);
            });
            return deferred.promise;
        };
        
        data.getCategoryByID = function(catId) {
            var deferred = $q.defer();
            category.orderByChild("cat_ID").equalTo(parseInt(catId)).once("value", function(data) {
              //console.log(data);
              
                //console.log(data);
                if(data){
                  var categorydataassoc = data.val();
                  //console.log(categorydataassoc);
                  var categorydata = null;
                  for (var key in categorydataassoc){
                    categorydata = categorydataassoc[key];
                    //console.log(categorydata);
                    deferred.resolve(categorydata);
                    //console.log(categorydata);
                    return;
                  }
                  //console.log(categorydata);
                }
                //console.log(categorydataassoc);
//                var categorydata = null;
//                for (categorydata in categorydataassoc)
//                  return;
//                console.log(categorydata);
//                data.forEach(function(value) {
//                    var key = value.key();
//                    var childData = value.val();
//                    childData.firebasekey = key;
//                    productdata.push(childData);
//                });
                
                //deferred.resolve(categorydata);
            });
            return deferred.promise;
        };
        data.updateLiveProducts = function(p_ID){
          var products = [];
          var liveRef1 = new Firebase(serverUrls.firebaseLivesale + '/products');
          liveRef1.once("value", function(data1) {
            products = data1.val();
            angular.forEach(products,function(value, key){
              if(value==data.p_ID){
                products.splice(key, 1);
                return;
              }
            });
            var productsnew = products.filter(function(){return true;});
            liveRef1.update(productsnew);
          });
        };
        data.updateProduct = function(FireId, data1) {
            if (FireId) {
                var prodRef = new Firebase(serverUrls.firebaseProduct + '/' + FireId);
                prodRef.update(data1);
                if(data1.p_is_frozen && data1.p_is_frozen=='Y'){
                  data.updateLiveProducts(data1.p_ID);
                }
            }
        };

        data.searchKeyword = function(searchTerm ,type) {
            var deferred = $q.defer();
            product.once("value", function(data) {
                var arr = [];
                data.forEach(function(ss) {
                    var strin = ss.val().p_title;
                    if (strin)
                    {
                        if (strin.match(new RegExp(searchTerm, 'gi')) && ss.val().p_is_frozen == 'N' && ss.val().p_on_live_sale == 'N' && ss.val().p_is_sold=='N')
                        {
                            /*Match found */
                            if(type=='keyword'){
                                arr.push(strin);
                            }
                            else if(type=='object')
                            {
                                arr.push(ss.val());
                            }
                        }
                    }
                });
                deferred.resolve(arr);
            });
            return deferred.promise;
        }
        
        data.getLivesaleParentCat = function(){
          //console.log('live slae cat');
          var deferred = $q.defer();
          product.orderByChild("available_in_livesale").equalTo(1).on("value", function(data) {
              var productdata = data.val();
              //console.log(productdata);
              //console.log(productdata);
//                data.forEach(function(value) {
//                    var key = value.key();
//                    var childData = value.val();
//                    childData.firebasekey = key;
//                    productdata.push(childData);
//                });
              var uniqueCat = {};
//              uniqueCat = _.uniq(productdata, function(item) {
//                  console.log(item);
//                  return item.parent_category_id;
//              });
//              console.log(uniqueCat);
              uniqueCat = _.map(_.groupBy(productdata,function(product){
                return product.parent_category_id;
              }),function(grouped){
                //console.log(grouped);
                return grouped[0];
              });
              $rootScope.$broadcast('catList', uniqueCat);
              deferred.resolve(uniqueCat);
          });
          return deferred.promise;
        };
        
        data.getLivesaleProducts = function(){
          //console.log('live sale product');
          var deferred = $q.defer();
          livesale.on("value", function(data) {
              var livesaledata = data.val();
              //console.log(livesaledata);
              //console.log(productdata);
//                data.forEach(function(value) {
//                    var key = value.key();
//                    var childData = value.val();
//                    childData.firebasekey = key;
//                    productdata.push(childData);
//                });
              //var uniqueCat = {};
//              uniqueCat = _.uniq(productdata, function(item) {
//                  console.log(item);
//                  return item.parent_category_id;
//              });
//              console.log(uniqueCat);
//              uniqueCat = _.map(_.groupBy(productdata,function(product){
//                return product.parent_category_id;
//              }),function(grouped){
//                //console.log(grouped);
//                return grouped[0];
//              });
//              console.log(uniqueCat);
              $rootScope.$broadcast('livesaleUpdate', livesaledata);
              deferred.resolve(livesaledata);
          });
          return deferred.promise;
        };
        
        data.getLivesaleCategoryProduct = function(catId){
          var livesaleCat = new Firebase(serverUrls.firebaseLivesale+'/live/'+catId+'/current');
          //console.log(serverUrls.firebaseLivesale+'/live/'+catId);
          var deferred = $q.defer();
          livesaleCat.on("value", function(data) {
              livesalecatdata[catId] = data.val();
              $rootScope.$broadcast('livesaleCatUpdate', livesalecatdata);
              deferred.resolve(livesalecatdata);
          });
          return deferred.promise;
        };
        
        data.updateLivesale = function(catId,data1) {
          var products = [];
          if(catId) {
            var liveRef = new Firebase(serverUrls.firebaseLivesale + '/live/' + catId+'/current');
            liveRef.update(data1);
            
            data.updateLiveProducts(data1.p_ID);
          }
        };

        data.getLiveSaleCategories = function() {
            var deferred = $q.defer();
            var liveRef1 = new Firebase(serverUrls.firebaseLivesale + '/live');
            liveRef1.once("value", function(data) {
                var livecategory = data.val();
                $rootScope.$broadcast('livecategories', livecategory);
                deferred.resolve(livecategory);
            });
            return deferred.promise;
        };
        return data;
    }
})();