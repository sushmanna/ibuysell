(function() {
    angular.module('ibuysell').factory('UserService', UserService);
    UserService.$inject = ['$http', 'serverUrls', '$window', '$location', '$rootScope','$q'];
    function UserService($http, serverUrls, $window, $location, $rootScope,$q) {
        var data = {};
        data.register = function(userData) {
            return $http({
                url: serverUrls.register,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.authRegister = function(userData) {
            return $http({
                url: serverUrls.insertuserbyauthid,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.checkUser = function(userData) {
            return $http({
                url: serverUrls.getuserbyauthid,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.login = function(userData) {
            return $http({
                url: serverUrls.login,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.forgotpassword = function(userData) {
            return $http({
                url: serverUrls.forgot,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.updateProfile = function(userData) {
            return $http({
                url: serverUrls.updateprofile,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.changePassword = function(userData) {
            return $http({
                url: serverUrls.changepassword,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.messageNotification = function(userData) {
            return $http({
                url: serverUrls.MessageCountNotification,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.localNotification = function(userData) {
            return $http({
                url: serverUrls.localCountNotification,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getLocalNotification = function(userData) {
            return $http({
                url: serverUrls.localNotification,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getAllNotification = function(userData) {
            return $http({
                url: serverUrls.getAllNotification,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getAllClosets = function(userData) {
            return $http({
                url: serverUrls.getAllClosets,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getSubscriptionPlan = function() {
            return $http({
                url: serverUrls.getsubscriptionplan,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getSubscriptionPlanById = function( data) {
            return $http({
                url: serverUrls.getsubscriptionplanbyid,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getSubscriptionById = function(userId) {
            var dd = {user_id: userId}
            return $http({
                url: serverUrls.getsubscriptionbyid,
                method: 'POST',
                data: dd,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.updateSubscriptionById = function(data) {
            return $http({
                url: serverUrls.updatesubscriptionplan,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getSellerById = function(userId) {
            var data = {user_id: userId}
            return $http({
                url: serverUrls.getseller,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getTagListByUser = function(userId) {
            var data = {user_id: userId};
            return $http({
                url: serverUrls.gettaglistByuser,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getCartCountByUser = function(userId) {
            var data = {buyer_id: userId};
            return $http({
                url: serverUrls.getcartcountbyuser,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getFeedbackById = function(data) {
            return $http({
                url: serverUrls.getfeedback,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getOrderById = function(userId) {
            var data = {user_id: userId};
            return $http({
                url: serverUrls.getorderbyid,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.setRatingByProductId = function(ratingData) {
            return $http({
                url: serverUrls.setfeedbackrating,
                method: 'POST',
                data: ratingData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getMessageById = function(data) {
            return $http({
                url: serverUrls.getmessagedetails,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getInbox = function(data) {
            return $http({
                url: serverUrls.getinboxmessage,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.sendMessageOrSupport = function(data) {
            return $http({
                url: serverUrls.setmessage,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.verifyCoupon = function(data) {
            return $http({
                url: serverUrls.verifycoupon,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.applyCoupon = function(data) {
            return $http({
                url: serverUrls.applycoupon,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.setImage = function(userData) {
            var imageURI = userData.profile_image;
            var deferred = $q.defer();
            var ft = new FileTransfer(),options = new FileUploadOptions();
            options.fileKey = "userimg";
            options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            var extention = options.fileName.split('.').pop();
            if (extention == 'jpeg' || extention == 'jpg')
            {
                options.mimeType = "image/jpeg";
            }
            else if (extention == 'png')
            {
                options.mimeType = "image/png";
            }
            else
            {
                options.mimeType = "image/jpeg";
            }
            var params = new Object();
            params.user_id = userData.userId;
            options.params = params;
            ft.upload(imageURI, serverUrls.uploadprofileimage, win, fail, options);
            function win(r) {
                deferred.resolve(r);
            }
            function fail(error) {
                deferred.reject(error);
            }
            return deferred.promise;
        };
        data.getCms = function(key) {
            var data={cms_key:key};
            return $http({
                url: serverUrls.getcms,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getMainFaqList = function() {
            return $http({
                url: serverUrls.mainfaqlist,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getFaqList = function() {
            return $http({
                url: serverUrls.faqlist,
                method: 'GET',
                responseText: 'JSON'
            });
        };

        /*update and get user address (Sandip Start)*/

        data.updateAddress = function(userData) {
            return $http({
                url: serverUrls.updateaddress,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getAllAddress = function(userData) {
            return $http({
                url: serverUrls.getaddress,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.checkverify = function(userData) {
            return $http({
                url: serverUrls.getuserverify,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.shareAppApi = function(data) {
            return $http({
                url: serverUrls.shareappapi,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.updateReferel = function(data) {
            return $http({
                url: serverUrls.submitrefferelcode,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.getSoldItemsById = function(userId) {
            var data = {user_id: userId};
            return $http({
                url: serverUrls.getsolditem,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.SetTrackCode = function(data) {
            return $http({
                url: serverUrls.settrackcode,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.alertNotification = function(data) {
            return $http({
                url: serverUrls.alertNotification,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        /*(sandip End)*/
        data.countrylist = function() {
            return $http({
                url: serverUrls.countrylist,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.accountpopdata = function() {
            return $http({
                url: serverUrls.accountpopdata,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        return data;
    }
})();
