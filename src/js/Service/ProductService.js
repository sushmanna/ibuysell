(function() {
    angular.module('ibuysell').factory('ProductService', ProductService);
    ProductService.$inject = ['$http', 'serverUrls', '$window', '$location', '$q'];
    function ProductService($http, serverUrls, $window, $location, $q) {
        var data = {};
        data.getCategories = function(userData) {
            return $http({
                url: serverUrls.getcategories,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getSizeBrand = function() {
            return $http({
                url: serverUrls.getsizebrand,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getSettings = function() {
            return $http({
                url: serverUrls.getsettings,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getShippingMethods = function() {
            return $http({
                url: serverUrls.getshippingmethods,
                method: 'GET',
                responseText: 'JSON',
                transformResponse: function(data, headersGetter, status) {
                    //var ValidJson = preg_replace("/(\n[\t ]*)([^\t ]+):/", "$1\"$2\":", data);
                    return {content: JSON.parse(JSON.stringify(data))};
                }
            });
        }; 
        data.getRelatedproductById = function(Product_id,User_id) {
            var data = {p_ID : Product_id, u_Id: User_id };
            return $http({
                url: serverUrls.getrelatedproductbyid,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.uploadProduct = function(productData) {
            return $http({
                url: serverUrls.uploadproduct,
                method: 'POST',
                data: productData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });

        };
        data.uploadProductImage = function(productData) {
            var imageURI = productData.imageUrl;
            var deferred = $q.defer();

            var ft = new FileTransfer(),
                    options = new FileUploadOptions();

            options.fileKey = "productimg";
            options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            var extention = options.fileName.split('.').pop();
            if (extention == 'jpeg' || extention == 'jpg')
            {
                options.mimeType = "image/jpeg";
            }
            else if (extention == 'png')
            {
                options.mimeType = "image/png";
            }
            else
            {
                options.mimeType = "image/jpeg";
            }
            var params = new Object();
            params.productId = productData.product_id;
            params.counter = productData.counter;
            options.params = params;
//            options.headers = {
//                "Connection": "close"
//            };
            ft.upload(imageURI, serverUrls.uploadproductimage, win, fail, options);

            function win(r) {
                deferred.resolve(r);
            }
            function fail(error) {
                deferred.reject(error);
            }
            return deferred.promise;
        };
        data.setPresetValue = function(presetData) {
            return $http({
                url: serverUrls.setpresetvalue,
                method: 'POST',
                data: presetData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });

        };
        data.removePresetValue = function(presetData) {
            return $http({
                url: serverUrls.removepresetvalue,
                method: 'POST',
                data: presetData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });

        };
        data.getPresetCount = function(productId,userId) {
            var data={p_ID:productId,u_Id:userId};
             return $http({
                url: serverUrls.getpresetcount,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.cartProducts = function(data) {
             return $http({
                url: serverUrls.cartDetails,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.removeCartItem = function( data){
            return $http({
                url: serverUrls.deleteCartItem,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.saveCartTempSave = function( data){
            return $http({
                url: serverUrls.cartTempSave,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.saveCartTempPaymentSave = function( data){
            return $http({
                url: serverUrls.cartTempPaymentSave,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.orderPaymentCheck = function( data){
            return $http({
                url: serverUrls.orderPaymentCheck,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.searchKeyword = function(keyword) {
            var data={keyword:keyword};
             return $http({
                url: serverUrls.getpresetcount,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.addToCart = function(data) {
             return $http({
                url: serverUrls.insertcart,
                method: 'POST',
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.updateProduct = function(freezeData) {
            return $http({
                url: serverUrls.updateProduct,
                method: 'POST',
                data: freezeData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });

        };
        data.getItemCondition = function() {
            return $http({
                url: serverUrls.getitemcondition,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });

        };
        return data;
    }
})();
