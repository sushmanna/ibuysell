(function () {
//var Twitter = require('twitter');
    angular.module('ibuysell')
            .directive('categoryslider', categoryslider);
    function  categoryslider()
    {
        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.templateUrl = "js/Directives/templates/categoryslider.html";
        directive.link = link;
        directive.scope = {slider:'=',category:'='};
        function link(scope, element, attrs)
        {
            if(attrs.detailstype=='live')
            {
                scope.link = 'liveproductdetail';
                scope.type = 'live';
            }else
            {
                scope.link = 'productdetail';
                scope.type = 'normal' ;
            }
            scope.sliders=scope.slider;
            scope.category=scope.category;
        }
        return directive;
    }
})();