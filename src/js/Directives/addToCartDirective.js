(function() {
    angular.module('ibuysell')
            .directive('addToCart', addToCart);
    addToCart.$inject = ['$localStorage', 'ionicToast', '$state', 'ProductService']
    function  addToCart($localStorage, ionicToast, $state, ProductService)
    {
        var directive = {};
        directive.restrict = 'A';
        directive.link = link;
        function link(scope, element, attrs)
        {
           
            var clickingCallback = function() {
                 console.log(attrs);
                var data = {buyer_id: $localStorage.loginData.user_ID, product_id: attrs.productid, price: attrs.price, product_title: attrs.producttitle};
                ProductService.addToCart(data).success(function(response, status)
                {
                    if (status == 200) {
                        ionicToast.show(response.msg, 'top', false, 2500);
                        $state.go('app.cart');
                    } else
                    {
                        ionicToast.show(response.msg, 'top', false, 2500);
                    }
                });
            };
            element.bind('click', clickingCallback);
        }
        return directive;
    }
})();
