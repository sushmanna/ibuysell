(function() {
    angular.module('ibuysell')
            .directive('managepreset', managepreset);
    managepreset.$inject = ['$localStorage', 'ionicToast', '$state', 'ProductService', '$ionicPopup', 'UserService', '$ionicLoading']
    function  managepreset($localStorage, ionicToast, $state, ProductService, $ionicPopup, UserService, $ionicLoading)
    {
        var directive = {};
        directive.restrict = 'A';
        directive.link = link;
        directive.scope = {userids: '@', counter: '@'};
        function link(scope, element, attrs)
        {


            function initclasscounter(userId, counter)
            {
                var cond = getcondition(userId, counter);
                if (cond)
                {
                    element.addClass("ion-heart");
                    element.html(counter);
                } else
                {
                    element.addClass("ion-ios-heart-outline");
                    element.html(counter);
                }
            }
            function setCounterAndIdHtml(id, counter)
            {
                element.attr("userids", id);
                element.attr("counter", counter);
                element.html(' '+counter);
            }


            function getcondition(userId, counter) {
                var userArray;
                if (userId)
                {
                    userArray = JSON.parse("[" + userId + "]");
                } else
                {
                    userArray = [];
                }
                var even = _.find(userArray, function(num) {
                    return $localStorage.loginData.user_ID == num;
                });
                if (even)
                {
                    return 1;
                } else
                {
                    return 0;
                }

            }
            var clickingCallback = function() {

                var condition = getcondition(scope.userids, attrs.counter);
                if (condition)
                {
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Remove reminder',
                        template: 'Are you sure you remove this reminder?'
                    });
                    confirmPopup.then(function(res) {
                        if (res) {
                            $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                            var data = {user_id: $localStorage.loginData.user_ID, product_id: attrs.productid};
                            ProductService.removePresetValue(data).success(function(response, status)
                            {
                                if (status == 200) {
                                    ionicToast.show(response.msg, 'top', false, 2500);
                                    element.removeClass("ion-ios-heart");
                                    element.addClass("ion-ios-heart-outline");
                                    scope.userids = response.data.userids;
                                    setCounterAndIdHtml(response.data.userids, response.data.counter);
//                                    var getid = JSON.parse("[" + scope.userids + "]");
//                                    getid = _.reject(getid, function(item) {
//                                        return item === $localStorage.loginData.user_ID;
//                                    });
//                                    var string = JSON.stringify(getid);
//                                    string = string.replace(']', '').replace('[', '');
//                                    scope.userids = string;
//                                    setCounterAndIdHtml(string, response.data);
                                }
                                $ionicLoading.hide();
                            });
                        }
                    });
                }
                else
                {
                    $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                    var data = {p_ID: attrs.productid, user_id: $localStorage.loginData.user_ID, preset_value: 0};
                    ProductService.setPresetValue(data).success(function(response, status)
                    {
                        if (status == 200) {
                            ionicToast.show(response.msg, 'top', false, 2500);
                            element.removeClass("ion-ios-heart-outline");
                            element.addClass("ion-ios-heart");
//                            var ids;
//                            if (attrs.userids)
//                            {
//                                ids = $localStorage.loginData.user_ID;
//                            }
//                            else
//                            {
//                                ids = attrs.userids + ',' + $localStorage.loginData.user_ID;
//                            }
                            scope.userids = response.data.userids;
                            setCounterAndIdHtml(response.data.userids, response.data.counter);
                        }
                        $ionicLoading.hide();
                    });
                }
            };
            element.bind('click', clickingCallback);
            initclasscounter(scope.userids, attrs.counter);
        }
        return directive;
    }
})();
