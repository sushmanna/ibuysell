(function () {
    angular.module('ibuysell')
            .directive('liveProgress', liveProgress);
    function  liveProgress()
    {

        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.template = '<div id="progressbar"><div class="progress-label">Loading...</div></div>';
        directive.link = link;
        directive.scope = {ngModel: '='};
        function link(scope, element, attrs)
        {
            var progressbar = jQuery("#progressbar");
            var progressLabel = jQuery(".progress-label");

            progressbar.progressbar({
                value: parseInt(attrs.max),
                change: function () {
                    progressLabel.text(progressbar.progressbar("value"));
                },
                complete: function () {
                    progressLabel.text("Complete!");
                }
            });

            function progress() {
                var min = parseInt(attrs.min);
                var max = parseInt(attrs.max);
                var val = progressbar.progressbar("value") || 0;
                progressbar.progressbar("value", val - 1);

                if (val > 0) {
                    setTimeout(progress, 1000);
                    scope.ngModel = scope.ngModel-1;
                }
            }

            setTimeout(progress, 2000);
        }
        return directive;
    }
})();
