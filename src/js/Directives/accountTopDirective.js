(function () {
//var Twitter = require('twitter');
    angular.module('ibuysell')
            .directive('accountTop', accountTop);
    function  accountTop()
    {
        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.templateUrl = "js/Directives/templates/accounttop.html";
        directive.link = link;
        directive.scope = {tab: '='};
        function link(scope, element, attrs)
        {
            scope.tab = attrs.tab 
        }
        return directive;
    }
})();
