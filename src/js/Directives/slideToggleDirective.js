(function(){
//var Twitter = require('twitter');
    angular.module('ibuysell')
            .directive('slidetoggle', slidetoggle);
    slidetoggle.$inject = ['$ionicModal', '$ionicLoading', 'UserService', 'ionicToast', '$localStorage'];
    function  slidetoggle($ionicModal, $ionicLoading, UserService, ionicToast, $localStorage)
    {
        return {
            restrict: 'A',
            scope: {
                entry: '=slidetoggle'
            },
            templateUrl: "js/Directives/templates/slideToggle.html",
            transclude: true,
            link: function(scope, element, attrs){
                scope.senderuserid = $localStorage.loginData.user_ID;
                scope.userid = parseInt(attrs.sellerid);
                var p = jQuery(element.find('div'));
                scope.clicked = function($event){
                    p.slideToggle();
                };
                /*set message modal*/
                $ionicModal.fromTemplateUrl('js/module/product/templates/messageModal.html', {
                    scope: scope
                }).then(function(modal){
                    scope.messageModal = modal;
                });
                //close messageModal modal
                scope.closeMessageModal = function(){
                    scope.messageModal.hide();
                };
                //open modal for messageModal
                scope.openMessageModal = function(){
                    scope.message = '';
                    scope.messageModal.show();
                };
                //send messageModal
                scope.submitMessage = function(msgData)
                {
                    $ionicLoading.show({template: '<img src="img/loader.GIF">'});
                    var data = {posted_to: parseInt(attrs.sellerid), posted_by: $localStorage.loginData.user_ID, msg_content: msgData.message, msg_type: 'message', msg_subject: msgData.subject};
                    UserService.sendMessageOrSupport(data).success(function(response, status)
                    {
                        $ionicLoading.hide();
                        scope.messageModal.hide();
                        if(status == 200){
                            ionicToast.show(response.msg, 'top', false, 2500);
                        }
                        else
                        {
                            ionicToast.show(response.msg, 'top', false, 2500);
                        }
                    });
                };
            }
        };
    }
})();
