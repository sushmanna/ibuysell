(function(){
    angular.module('ibuysell')
            .directive('myRedirection', myRedirection);
    myRedirection.$inject = ['$window']
    function  myRedirection($window)
    {
        var directive = {};
        directive.restrict = 'A';
        directive.link = link;
        function link(scope, element, attrs)
        {
            var clickingCallback = function(){
                $window.location.href = attrs.myRedirection;
            };
            element.bind('click', clickingCallback);
        }
        return directive;
    }
})();
