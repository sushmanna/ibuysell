(function() {
    angular.module('ibuysell')
            .directive('accountFooter', accountFooter);
    accountFooter.$inject = ['$ionicActionSheet', '$location', 'ionicToast'];
    function  accountFooter($ionicActionSheet, $location, ionicToast)
    {
        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.templateUrl = "js/Directives/templates/accountfooter.html";
        directive.link = link;
        //directive.scope = {mapdata: '=', center: '=', zoom: '='};
        function link(scope, element, attrs)
        {
            scope.tab = attrs.tab
            scope.openFooterMenu = function()
            {
                $ionicActionSheet.show({
                    buttons: [
                        {text: '<i class="ion-ios-gear"></i> Account Setting'},
                        {text: '<i class="ion-clipboard"></i> My Order'},
                        {text: '<i class="ion-clipboard"></i> Sold Item'},
                        {text: '<i class="ion-compose"></i> Feedbacks'},
                        {text: '<i class="ion-android-mail"></i> Inbox'},
                        {text: '<i class="ion-android-notifications"></i>Product Reminder List'},
                        {text: '<i class="ion-android-contacts"></i> Customer Support'},
//                        {text: '<i class="ion-android-bulb"></i> Tutorial'},
                        {text: '<i class="ion-ios-paper"></i> FAQ'},
                        {text: '<i class="ion-help-circled"></i> Help Center'}
                    ],
                    //destructiveText: 'Delete',
                    //titleText: 'Modify your album',
                    //cancelText: 'Cancel',
                    cancel: function() {
                        // add cancel code..
                    },
                    buttonClicked: function(index) {
                        switch (index) {
                            case 0:
                                $location.path('app/mysetting');
                                break;
                            case 1:
                                $location.path('app/myorder');
                                break;
                            case 2:
                                $location.path('app/solditem');
                                break;
                            case 3:
                                $location.path('app/myfeedback/');
                                break;
                            case 4:
                                $location.path('app/inbox');
                                break;
                            case 5:
                                $location.path('app/taglist');
                                break;
                            case 6:
                                $location.path('app/customersupport');
                                break;
//                            case 5:
//                                ionicToast.show('The page is under construction.', 'top', false, 2500);
//                                break;
                            case 7:
                                $location.path('app/faq');
                                break; 
                            case 8:
                                $location.path('app/help');
                                break;
                        }
                        return true;
                    }
                });
            };
        }
        return directive;
    }
})();
