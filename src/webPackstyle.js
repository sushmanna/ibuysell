define(function (require, exports, module) {
    //style html
    require("./lib/ionic/css/ionic.min.css");
    require("./lib/ionic-toast/src/style.css");
    require("./lib/swiper/dist/css/swiper.min.css");
  //  require("./lib/ion-autocomplete/dist/ion-autocomplete.min.css");
    require("./css/icon.css");
    require("./lib/jquery-ui-1.11.4.custom/jquery-ui.css");
    require("./lib/ionic-rating/ionic-rating.css");
    require("./css/style.css");
});